;Crie uma expressão Lambda que dados dois pontos no espaço 3D, (x1, y1, z1) e (x2, y2, z2), compute a
;distância entre eles. Leia as posições dos pontos do teclado.

;OK

(defun distancia (xa ya za xb yb zb)
	((lambda (xa ya zb xb yb zb) 
		(sqrt (+ (expt (- xb xa) 2) (expt (- yb ya) 2) (expt (- zb za) 2)))) xa ya za xb yb zb)
)

(defun main ()
	(write-line (write-to-string (distancia 4 (- 8) (- 9) 2 (- 3) (- 5))))
)

(main)