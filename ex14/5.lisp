; Crie uma expressão Lambda que receba 3 valores numéricos (a, b, c) e retorne o maior deles. Não utilize
; nenhuma forma de ordenação. Leia os valores a, b, c do teclado.

;OK

(defun maior (a b c)
	((lambda (a b c) 
		(if (and (> a b) (> a c)) 
			a
			(if (and (> b a) (> b c))
				b
				c
			)
		)
	) a b c)
)

(defun main ()
	(write-line (write-to-string (maior 1 3 2)))
)

(main)