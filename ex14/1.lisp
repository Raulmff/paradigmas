; Crie uma expressão Lambda que receba dois valores booleanos (x, y) retorne o resultado do “ou exclusivo”
; (XOR) sobre eles. Leia os valores x e y do teclado.

;OK

(defun XOR (x y)
	((lambda (x y) (if x (not y) y)) x y)
)

(defun main ()
	(write-line (write-to-string (XOR T T)))
	(write-line (write-to-string (XOR T NIL)))
	(write-line (write-to-string (XOR NIL T)))
	(write-line (write-to-string (XOR NIL NIL)))
)

(main)