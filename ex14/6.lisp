; Utilize a função mapear criada em aulas anteriores de modo a receber como parâmetros uma lista
; numérica e uma função lambda e retorna uma lista com os resultados da aplicação da função lambda em
; cada elemento da lista. A função lambda deve retornar par ou ı́mpar para cada número, ou seja, uma
; lista de booleanos.

;OK

(defun aplica (f lista)
	(if (not(null lista))
		(cons (funcall f (first lista)) (aplica f (rest lista)) )
		'()
	)
)

(defun main()
	(write-line (write-to-string (aplica (lambda (x) (= (mod x 2) 0)) '(1 2 3 4))))
)

(main)