; Utilize a função filtrar criada em aulas anteriores de modo a receber como parâmetros uma lista numérica e
; uma função lambda e retorna uma lista com os resultados da aplicação da função lambda em cada elemento
; da lista. A função lambda deve retornar se um número é ı́mpar, assim o resultado da chamada da função
; filtrar com esta função lambda deve ser uma lista dos números ı́mpares.

(defun impares (f lista)
	(if (not(null lista))
		(if (funcall f (first lista))
			(cons (first lista) (impares f (rest lista)))
			(impares f (rest lista))
		)
		'()
	)
)

(defun main()
	(write-line (write-to-string (impares (lambda (x) (/= (mod x 2) 0)) '(1 2 3 4 5 6 7 8 9))))
)

(main)