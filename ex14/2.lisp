; Crie uma expressão Lambda que receba três notas de um aluno (a, b, c), calcule a média e retorne se o
; aluno foi aprovado ou reprovado. Para um aluno ser aprovado, ele deve possuir nota igual ou superior a
; 6. Leia as notas dos alunos do teclado.

;OK

(defun situacao (a b c)
	( (lambda (a b c) (if (>= (/ (+ a b c) 3) 6) T NIL)) a b c )
)

(defun main ()
	(write-line (write-to-string (situacao 5 5 5)))
	(write-line (write-to-string (situacao 7 8 9)))
	(write-line (write-to-string (situacao 4 2 10)))
	(write-line (write-to-string (situacao 8 9 9)))
)

(main)