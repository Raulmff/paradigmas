; Crie uma expressão Lambda que resolva uma equação de segundo grau da forma ax 2 + bx + c utilizando
; a fórmula de Bhaskara. Leia os coeficientes a, b e c do teclado.

;OK

(defun bhaskara1 (a b c)
	;(( lambda (a b c) (/ (+ b ((sqrt (- (expt b 2) (* 4 a c))))) (* 2 a)) ) a b c )
	;((lambda (a b c) (- (expt b 2) (* 4 a c))) a b c)
	((lambda (a b c) (/(+ (- b) (sqrt (- (expt b 2) (* 4 a c))))(* 2 a))) a b c)
)

(defun bhaskara2 (a b c)
	((lambda (a b c) (/(- (- b) (sqrt (- (expt b 2) (* 4 a c))))(* 2 a))) a b c)
)

(defun main ()
	(write-line (write-to-string (bhaskara1 1 12 (- 13))))
	(write-line (write-to-string (bhaskara2 1 12 (- 13))))
	(write-line (write-to-string (bhaskara1 2 (- 16) (- 18))))
	(write-line (write-to-string (bhaskara2 2 (- 16) (- 18))))
)

(main)