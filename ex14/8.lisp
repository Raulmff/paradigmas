;Dizemos que uma matriz quadrada inteira A nxn é um quadrado mágico se é formada pelos números (não
;repetidos) de 1 à n 2 e a soma dos elementos de cada linha, a soma dos elementos de cada coluna e a
;soma dos elementos das diagonais principal e secundária são todas iguais. O exemplo abaixo mostra um
;quadrado mágico válido. Faça um função que retorne se uma matriz é um quadrado mágico ou não.

(setf matriz (make-array '(3 3) ;criando uma matriz 3x3
   :initial-contents '((2 7 6) (9 5 1) (4 3 8))) ;inicializando a matriz com alguns valores
)

(setf MAXIMO (expt 3 2))

;FUNCIONA
(defun conferirNaoRepetidos (m lista retorno)
    (dotimes (i 3) ;dotimes permite realizar um número fixo de iterações
        (dotimes (j 3)
            (if (member (aref m i j) lista)
                (setf retorno NIL)
                (setf lista (cons (aref m i j) lista))
            )
        )
    )
    retorno
)

;FUNCIONA
(defun conferirMenoresQueMaximo (m retorno)
    (dotimes (i 3) ;dotimes permite realizar um número fixo de iterações
        (dotimes (j 3)
            (if (or (> (aref m i j) MAXIMO) (< (aref m i j) 1))
                (setf retorno NIL)
            )
        )
    )
    retorno
)

;FUNCIONA
(defun somaColuna (m soma retorno)
    (dotimes (i 3) ;dotimes permite realizar um número fixo de iterações
    	(progn
    		(setf soma 0)
	        (dotimes (j 3)
	            (setf soma (+ soma (aref m i j)))
	        )
	        (setf retorno (cons soma retorno))
	    )
    )
    retorno
)

(defun somaLinha (m soma retorno)
    (dotimes (i 3) ;dotimes permite realizar um número fixo de iterações
        (progn
            (setf soma 0)
            (dotimes (j 3)
                (setf soma (+ soma (aref m j i)))
            )
            (setf retorno (cons soma retorno))
        )
    )
    retorno
)

(defun somaDiagonalPrincipal (m soma)
    (dotimes (i 3) ;dotimes permite realizar um número fixo de iterações
        (progn
            (dotimes (j 3)
                (if (= i j)
                    (setf soma (+ soma (aref m j i)))
                )
            )
        )
    )
    soma
)

(defun somaDiagonalSecundaria (m soma i j)
    (if (not(= j 0))
        (somaDiagonalSecundaria m (+ soma (aref m i j)) (+ i 1) (- j 1))
        (+ soma (aref m i j))
    )
)

(setf NUMEROASERIGUAL (somaDiagonalPrincipal matriz 0))

(defun verificaSoma (lista)
    (if (not (null lista))
        (if (= (first lista) NUMEROASERIGUAL)
            (and T (verificaSoma (rest lista)))
            (and NIL (verificaSoma (rest lista)))
        )
        T
    ) 
)

(defun verificaSomaDiagonal (soma)
    (if (= soma NUMEROASERIGUAL)
        T
        NIL
    ) 
)

(defun verificarTudoIgual ()
	(and 
        (conferirNaoRepetidos matriz '() T)
        (conferirMenoresQueMaximo matriz T)
        (verificaSoma (somaLinha matriz 0 '()))
        (verificaSoma (somaColuna matriz 0 '()))
        (verificaSomaDiagonal (somaDiagonalSecundaria matriz 0 0 2))   
    )
)

(defun quadradoMagico ()
    (if (verificarTudoIgual)
        "É um quadrado magico"
        "Não é um quadrado magico"
    )
)

(defun main()
    (write-line (write-to-string matriz)) ;imprimindo uma matriz
    ;(write-line (write-to-string (aref x 0 0)))
    ;(write-line (write-to-string (aref x 1 1)))
    ;(write-line (write-to-string (maior x))) ;passando uma matriz como parâmetro
    ;(setf (aref x 0 1) 33) ;alrerando o elemento da posição 0,1 para 33
    ;(write-line (write-to-string (maior x)))
    (write-line (write-to-string (quadradoMagico)))
)

(main)