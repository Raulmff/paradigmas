
-- Crie uma função que receba uma matriz e retorne a soma de seus elementos.

--OK

somaElemMatriz :: [[Int]] -> Int
somaElemMatriz [] = 0
somaElemMatriz (l:c) = (somaLista l) + (somaElemMatriz c)

somaLista :: [Int] -> Int
somaLista [] = 0
somaLista (a:b) = a + (somaLista b)

main = do
	print (somaElemMatriz [[1,2,3],[1,1,1]])
