-- Dizemos que uma matriz quadrada inteira A nxn é um quadrado mágico se é formada pelos números
-- (não repetidos) de 1 à n 2 e a soma dos elementos de cada linha, a soma dos elementos de cada coluna
-- e a soma dos elementos das diagonais principal e secundária são todas iguais. O exemplo abaixo
-- mostra um quadrado mágico válido. Faça um função que retorne se uma matriz é um quadrado
-- mágico ou não.

--OK
diferentes :: [[Int]] -> [Int] -> Bool
diferentes [] _ = True
diferentes (l:c) x = (diferentesLinha l x) && (diferentes c (l++x))

--OK
diferentesLinha :: [Int] -> [Int] -> Bool
diferentesLinha [] _ = True
diferentesLinha (a:b) l2 = 
	if (a `elem` l2)
		then False
		else (diferentesLinha b ([a]++l2))

--OK
quadrada :: [[Int]] -> Bool
quadrada m = 
	if ((nLinhas m) == (nColunas m))
		then True
		else False

--OK
nLinhas :: [[Int]] -> Int
nLinhas [] = 0
nLinhas (l:c) = 1 + (nLinhas c)

--OK
nColunas :: [[Int]] -> Int
nColunas (l:c) = (nElementos l)

--OK
nElementos :: [Int] -> Int
nElementos [] = 0
nElementos (a:b) = 1 + (nElementos b)

--OK
nQuadrado :: [[Int]] -> Int -> Bool
nQuadrado m1 n = (nQuadrado2 (matrizParaVetor m1) n)

--OK
nQuadrado2 :: [Int] -> Int -> Bool
nQuadrado2 [] _ = True
nQuadrado2 (a:b) n =
	if ( a > (n*n) )
		then False
		else (nQuadrado2 b n)

-- OK
matrizParaVetor :: [[Int]] -> [Int]
matrizParaVetor [] = []
matrizParaVetor (l:c) = l ++ (matrizParaVetor c)

-- tudoIgual :: [t] -> Bool

-- somasIguais :: [[Int]] -> Bool
-- somasIguais m = (tudoIgual (somasLinhas)) && ...

-- somasLinhas :: [[Int]] -> [Int]
-- somasLinhas [] = []
-- somasLinhas (l:c) = [(somaLinha l)] ++ (somasLinhas c)

-- somaLinha :: [Int] -> Int
-- somaLinha [] = 0
-- somaLinha (a:b) = a + (somaLinha b)

-- somasColunas :: [[Int]] -> Int

-- somaColuna :: [Int] -> Int
-- somaColuna [] = 0
-- somaColuna (a:b) = a + (somaColuna b)


-- quadradoMagico :: [[Int]] -> Bool
-- quadradoMagico m = 
-- 	if ((diferentes m []) && (quadrada m) && (nQuadrado m) && (somasIguais m))
-- 		then True
-- 		else False

main = do
	let m1 = [[2,7,6],[9,5,1],[4,3,8]]
	print (nQuadrado m1 (nLinhas m1))
