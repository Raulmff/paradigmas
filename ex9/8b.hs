
-- Crie uma função que receba duas matrizes e retorne a soma delas.

--OK

nLinhas :: [[Int]] -> Int
nLinhas [] = 0
nLinhas (l:c) = 1 + (nLinhas c)

nColunas :: [[Int]] -> Int
nColunas (l:c) = (nElementos l)

nElementos :: [Int] -> Int
nElementos [] = 0
nElementos (a:b) = 1 + (nElementos b)

somaMatrizes :: [[Int]] -> [[Int]] -> [[Int]]
somaMatrizes m1 m2 = 
	if (((nColunas m1) == (nColunas m2)) && ((nLinhas m1) == (nLinhas m2)))
		then (soma m1 m2)
		else []

soma :: [[Int]] -> [[Int]] -> [[Int]]
-- a1 e a2 sao linhas
soma [] [] = []
soma (a1:b1) (a2:b2) = (somaLinhas a1 a2) : (soma b1 b2)

somaLinhas :: [Int] -> [Int] -> [Int]
somaLinhas [] [] = []
somaLinhas (a1:b1) (a2:b2) = [(a1+a2)] ++ (somaLinhas b1 b2)

main = do
	let m1 = [[5,4],[0,2],[1,(-1)]]
	let m2 = [[0,(-2)],[5,(-3)],[(-1),0]]
	print (somaMatrizes m1 m2)
