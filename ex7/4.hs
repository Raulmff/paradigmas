-- Faca um exemplo ilustrativo utilizando os m´etodos ceiling e floor da classe RealFrac

main = do
	print "ceiling e floor de 1/3, respectivamente"
	print (ceiling (1/3))
	print (floor (1/3))

	print "ceiling e floor de 15/8, respectivamente"
	print (ceiling (15/8))
	print (floor (15/8))