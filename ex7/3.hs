-- Altere a classe MeuInt vista em sala de aula e disponı́vel nos slides no Moodle para suportar novos métodos
-- e operações, descritos abaixo:

class (Integral x) => MeuInt x where
	bigger  :: x -> x -> x
	smaller :: x -> x -> x
	par :: x -> Bool
	impar :: x -> Bool
	primo :: x -> Bool
	mdc :: x -> x -> x
	(===) :: x -> x -> Bool
	coprimos :: x -> x -> Bool
	(-+-) :: x -> x -> x

	bigger a b 
		| a > b = a
		| otherwise = b

	smaller a b 
		| a == (bigger a b) = b
		| otherwise = a

-- A: par :: x -> Bool - Este método deve retornar se um dado número é par ou não.
	par a
		| (a `mod` 2 == 0) = True
		| otherwise = False

-- B: impar :: x -> Bool - Este método deve retornar se um dado número é ı́mpar ou não.
	impar a = (not (par a))

-- C: primo :: x -> Bool - Este método deve retornar se um dado número é primo ou não.
	primo k = null [ x | x <- [2..k - 1], k `mod` x  == 0]

-- D: mdc :: x -> x -> x - Este método deve retornar o máximo divisor comum entre dois números.
	mdc a b
		| (b == 0) = a
		| otherwise = (mdc b (a `mod` b))
		
-- E: (===) :: x -> x -> Bool - Este operador deve retornar True se a diferença entre dois números
-- inteiros é igual ou menor que 1 e False se a diferença entre dois inteiros é maior que 1.
	(===) a b
		| ((a - b) <= 1) = True
		| otherwise = False 

-- F: Crie um novo método a sua escolha, explique seu funcionamento e exemplifique seu uso.
-- Retorna se dois elementos são coprimos ou não
	coprimos a b 
		| ((mdc a b) == 1) = True
		| otherwise = False
							
-- G: Crie um novo operador a sua escolha, explique seu funcionamento e exemplifique seu uso.
-- Retorna a soma de duas diferencas entre os dois parametros
	(-+-) a b = ((a-b) + (a-b))

instance MeuInt Integer
instance MeuInt Int

main = do 
	print (par (5 :: Int))
	print (impar (5 :: Int))
	print (primo (13 :: Int))
	print (mdc (16 :: Int) (24 :: Int))
	print ((42 :: Int) === (24 :: Int))
	print (coprimos (20 :: Int) (21 :: Int))
	print ((10 :: Int) -+- (3 :: Int))
