-- Fa¸ca um exemplo ilustrativo utilizando os m´etodos min e max da classe Ord

main = do
	print "valor minimo entre 1 e 5"
	print (min 1 5)

	print "valor maximo entre 1 e 5"
	print (max 1 5)