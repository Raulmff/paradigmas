:- dynamic corteTorra/2.
:- retractall( corteTorra(_,_) ).

corte(1,3).
corte(2,5).
corte(3,10).
corte(4,12).
corte(5,14).

corteTorra(0,0) :- !.

corteTorra(N,K) :-
N > 0,
findall(KPreco,(corte(Tam,Preco), Tam =< N, Resto is N - Tam, corteTorra(Resto,KPrecoAntigo), KPreco is KPrecoAntigo+Preco),ListaKPreco),
max_list(ListaKPreco, KMaxPreco),
K is KMaxPreco,
asserta(corteTorra(N,K) :- !).

