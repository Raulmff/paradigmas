% Relacao de filmes
%    filme(id, titulo, ano, diretor, nacionalidade).
%
filme(f1, 'Monty Python: O Sentido da Vida', 1983, 'Terry Jones', uk).
filme(f2, 'Edukators', 2004, 'Hans Weingartner', de).
filme(f3, 'Lavoura Arcaica', 2001, 'Luiz Fernando Carvalho', br).
filme(f4, 'Lisbela e o Prisioneira', 2003, 'Guel Arraes', br).
filme(f5, 'Abril despedaçado', 2001, 'Walter Salles', br).
filme(f6, 'Diários de motocicleta', 2004, 'Walter Salles', br).

% Relacao de paises
%     pais(sigla, nome).
%
pais(uk, 'Unided Kingdom').
pais(de, 'Alemanha').
pais(br, 'Brasil').

% Relacao de DVD (a caixa em si)
%     dvd(id do DVD, id do filme, estante).
%
dvd(d1, f1, est1).
dvd(d2, f2, est1).
dvd(d4, f4, est1).
dvd(d3, f3, est2).
dvd(d5, f5, est3).
dvd(d6, f1, est1).
dvd(d7, f2, est4).
dvd(d8, f2, est4).

% Relacao de clientes
%     cliente(cod, nome, telefone).
%
cliente(c1, 'Bob', '333-3112').
cliente(c2, 'Zeca', '245-1099').
cliente(c3, 'Tom', '323-0685').
cliente(c4, 'Bianca', '333-4391').
cliente(c5, 'Alice', '251-7439').
cliente(c6, 'Maria', '212-3271').

% Relacao de locacoes
%     locacao(cod cliente, nro do DVD, data de entrega)
%
locacao(c1, d1, '2005-11-07').
locacao(c1, d2, '2005-11-07').
locacao(c3, d5, '2005-11-09').
locacao(c2, d3, '2005-11-10').
locacao(c3, d3, '2005-11-11').
locacao(c4, d8, '2005-11-12').
locacao(c5, d7, '2005-11-12').
locacao(c6, d6, '2005-11-12').
locacao(c1, d5, '2005-11-13').
locacao(c1, d6, '2005-11-13').
locacao(c6, d2, '2005-11-14').
locacao(c3, d7, '2005-11-14').
locacao(c3, d8, '2005-11-14').
locacao(c5, d1, '2005-11-15').

%%%%%%%%(a) Retornar a lista de todos os tı́tulos de filmes lançados antes de 2001.

%?- findall(F,(filme(_,F,Ano,_,_),Ano<2001),Filmes).
%Filmes = ['Monty Python: O Sentido da Vida'].

%%%%%%%%(b) Retornar a lista de todos os tı́tulos de filmes lançados entre 2001 (inclusive) e 2004 (inclusive).

%?- findall(F,(filme(_,F,Ano,_,_), Ano>=2001, Ano=<2004),Filmes).
%Filmes = ['Edukators', 'Lavoura Arcaica', 'Lisbela e o Prisioneira', 'Abril despedaçado', 'Diários de motocicleta'].

%%%%%%%%(c) Retornar a lista de todos os tı́tulos de filmes, de maneira ordenada, produzidos no ’Brasil’.

%?- findall(F,(filme(_,F,_,_,Nac),Nac==br),Filmes),sort(Filmes,FilmesOrdenados).
%Filmes = ['Lavoura Arcaica', 'Lisbela e o Prisioneira', 'Abril despedaçado', 'Diários de motocicleta'],
%FilmesOrdenados = ['Abril despedaçado', 'Diários de motocicleta', 'Lavoura Arcaica', 'Lisbela e o Prisioneira'].

%%%%%%%%(d) Retornar a lista de todos os tı́tulos de filmes, de maneira ordenada, n~
%%%%%%%%ao produzidos no ’Brasil’.

%?- findall(F,(filme(_,F,_,_,Nac),not(Nac==br)),Filmes),sort(Filmes,FilmesOrdenados).
%Filmes = ['Monty Python: O Sentido da Vida', 'Edukators'],
%FilmesOrdenados = ['Edukators', 'Monty Python: O Sentido da Vida'].

%%%%%%%%(e) Retornar a lista de todos os tı́tulos e diretores dos filmes produzidos na ’Alemanha’ com ano de
%%%%%%%%lançamento é 2004 ou anterior.

%?- findall(F,(filme(_,F,Ano,Diretor,Nac),Nac==de,Ano=<2004),Filmes),findall(Diretor,(filme(_,F,Ano,Diretor,Nac),Nac==de,Ano=<2004),Diretores).
%Filmes = ['Edukators'],
%Diretores = ['Hans Weingartner'].

%%%%%%%%(f) Retornar a lista de todos os identificadores de DVD’s com filmes não produzidos no ’Brasil’.

%?- findall(DVD,( dvd(DVD,Filme,_), filme(Filme,_,_,_,Nac), not(Nac==br)),Dvds).
%Dvds = [d1, d2, d6, d7, d8].

%%%%%%%%(g) Retornar a lista de todos os identificadores de DVD’s com filmes produzidos no ’Brasil’ e que estão
%%%%%%%%na estante 2.

%?- findall(DVD,( dvd(DVD,Filme,Estante),filme(Filme,_,_,_,Nac),Nac==br,Estante==est2),Dvds).
%Dvds = [d3].

%%%%%%%%(h) Retornar a lista de todos os diretores de filmes, ordenados, cujos DVD’s estão na estante 1.

%?- findall(Diretor,( filme(Id,_,_,Diretor,_),dvd(_,Id,Est),Est==est1 ),Diretores),sort(Diretores,DiretoresOrdenados).
%Diretores = ['Terry Jones', 'Terry Jones', 'Hans Weingartner', 'Guel Arraes'],
%DiretoresOrdenados = ['Guel Arraes', 'Hans Weingartner', 'Terry Jones'].

%%%%%%%%(i) Retornar a lista de todos os tı́tulos de filmes que não possuem um DVD.

%?- findall(Filme,(filme(Id,Filme,_,_,_),not(dvd(_,Id,_))),Filmes).
%Filmes = ['Diários de motocicleta'].

%%%%%%%%(j) Retornar a lista de todos os nomes de paı́ses que possuem filmes com DVD’s nas estantes 1 ou 4.

%?- findall(Pais,(pais(Nac,Pais),filme(Id,_,_,_,Nac),dvd(_,Id,Est),(Est==est1;Est==est4)),Paises).
%Paises = ['Unided Kingdom', 'Unided Kingdom', 'Alemanha', 'Alemanha', 'Alemanha', 'Brasil'].

%%%%%%%%(k) Retornar a lista de todos os nomes de clientes que alugaram filmes no dia 7 de novembro de 2005.

%?- findall(Nome,(cliente(Id,Nome,_),locacao(Id,_,Data),Data=='2005-11-07'),Nomes).
%Nomes = ['Bob', 'Bob'].

%%%%%%%%(l) Retornar a lista de todas as estantes que possuem filmes que tiveram alguma locação.

%?- findall(Est,(locacao(_,IdD,_),filme(IdF,_,_,_,_),dvd(IdD,IdF,Est)),Estantes),sort(Estantes,EstantesOrd).
%Estantes = [est1, est1, est3, est2, est2, est4, est4, est1, est3|...],
%EstantesOrd = [est1, est2, est3, est4].

%%%%%%%%(m) Retornar a lista de tı́tulos de filmes alugados pela cliente ’Maria’.

%?- findall(Filme,(cliente(IdCliente,Nome,_),Nome=='Maria',locacao(IdCliente,IdDvd,_),dvd(
%IdDvd,IdFilme,_),filme(%IdFilme,Filme,_,_,_)),Filmes).Filmes = ['Monty Python: O Sentido da Vida', 'Edukators'].

%%%%%%%%(n) Retornar a quantidade de filmes diferentes alugados pela cliente ’Maria’.

%?- findall(Filme,(cliente(IdCliente,Nome,_),Nome=='Maria',locacao(IdCliente,IdDvd,_),dvd(IdDvd,IdFilme,_),filme(
%IdFilme,Filme,_,_,_)),Filmes),sort(Filmes,FilmesOrd),length(FilmesOrd,QuantidadeFilmes).
%Filmes = ['Monty Python: O Sentido da Vida', 'Edukators'],
%FilmesOrd = ['Edukators', 'Monty Python: O Sentido da Vida'],
%QuantidadeFilmes = 2.

%%%%%%%%(o) Retornar a lista de tı́tulos de filmes agrupados por estante, ou seja, para cada estante mostrar os
%%%%%%%%filmes nela.

%?- setof(Filme,IdDvd^IdFilme^QQ1^QQ2^QQ3^(dvd(IdDvd,IdFilme,Est),filme(IdFilme,Filme,QQ1,QQ2,QQ3)),Filmes).
%Est = est1,
%Filmes = ['Edukators', 'Lisbela e o Prisioneira', 'Monty Python: O Sentido da Vida'] ;
%Est = est2,
%Filmes = ['Lavoura Arcaica'] ;
%Est = est3,
%Filmes = ['Abril despedaçado'] ;
%Est = est4,
%Filmes = ['Edukators'].

%%%%%%%%(p) Retornar a lista de nomes de paı́ses que o cliente ’Bob’ alugou filmes no dia 7 de novembro de 2005.

%?- findall(Pais,(filme(IdFilme,_,_,_,Nac),pais(Nac,Pais),dvd(IdDvd,IdFilme,_),locacao(IdCliente,IdDvd,Data),Data=='2005-11-07',cliente(
%IdCliente,Nome,_),Nome=='Bob'),Paises).
%Paises = ['Unided Kingdom', 'Alemanha'].

%%%%%%%%(q) Retornar a quantidade de filmes produzidos no Brasil alugados por ’Bob’.

%?- findall(IdFilme,(filme(IdFilme,_,_,_,Nac),pais(Nac,NomePais),NomePais=='Brasil',dvd(IdDvd,IdFilme,_),locacao(
%IdCliente,IdDvd,_),cliente(IdCliente,Nome,_),Nome=='Bob'),Filmes),sort(Filmes,FilmesOrd),length(FilmesOrd,QuantidadeFilmes).
%Filmes = FilmesOrd, FilmesOrd = [f5],
%QuantidadeFilmes = 1.

%%%%%%%%(r) Retornar a lista de tı́tulos dos filmes (sem repetições) agrupados por cliente, ou seja, para cada cliente
%%%%%%%%mostrar os filmes alugados por ele.

%?- setof(Filme,IdCliente^Fone^IdDvd^IdFilme^Est^Data^Ano^Dir^Nac^(cliente(IdCliente,Nome,Fone),dvd(IdDvd,IdFilme,Est),locacao(
%IdCliente,IdDvd,Data),filme(IdFilme,Filme,Ano,Dir,Nac)),Filmes).
%Nome = 'Alice',
%Filmes = ['Edukators', 'Monty Python: O Sentido da Vida'] ;
%Nome = 'Bianca',
%Filmes = ['Edukators'] ;
%Nome = 'Bob',
%Filmes = ['Abril despedaçado', 'Edukators', 'Monty Python: O Sentido da Vida'] ;
%Nome = 'Maria',
%Filmes = ['Edukators', 'Monty Python: O Sentido da Vida'] ;
%Nome = 'Tom',
%Filmes = ['Abril despedaçado', 'Edukators', 'Lavoura Arcaica'] ;
%Nome = 'Zeca',
%Filmes = ['Lavoura Arcaica'].

%%%%%%%%(s) Retornar a lista de tı́tulos dos filmes alugados por ambos ’Zeca’ e ’Tom’, ou seja, apenas os filmes
%%%%%%%%em comum alugados por ambos.

%?- findall(Filme,(filme(IdFilme,Filme,_,_,_),dvd(IdDvd,IdFilme,_),locacao(IdCliente,IdDvd,_),cliente(
%IdCliente,Nome,_),Nome=='Zeca'),FilmesZeca),findall(Filme,(filme(IdFilme,Filme,_,_,_),dvd(IdDvd,IdFilme,_),locacao(
%IdCliente,IdDvd,_),cliente(IdCliente,Nome,_),Nome=='Tom'),FilmesTom),(member(FilmesEmComum,FilmesZeca),member(FilmesEmComum,FilmesTom)).
%FilmesZeca = ['Lavoura Arcaica'],
%FilmesTom = ['Edukators', 'Edukators', 'Lavoura Arcaica', 'Abril despedaçado'],
%FilmesEmComum = 'Lavoura Arcaica' .

%%%%%%%%(t) Retornar a lista de tı́tulos dos filmes alugados por ’Bob’, mas não alugados por ’Maria’.

%?- findall(Filme,(filme(IdFilme,Filme,_,_,_),dvd(IdDvd,IdFilme,_),locacao(IdCliente,IdDvd,_),cliente(
%IdCliente,Nome,_),Nome=='Bob'),FilmesBob),findall(Filme,(filme(IdFilme,Filme,_,_,_),dvd(IdDvd,IdFilme,_),locacao(
%IdCliente,IdDvd,_),cliente(IdCliente,Nome,_),Nome=='Maria'),FilmesMaria),(member(FilmesEmComum,FilmesBob),not(member(
%FilmesEmComum,FilmesMaria))).
%FilmesBob = ['Monty Python: O Sentido da Vida', 'Monty Python: O Sentido da Vida', 'Edukators', 'Abril despedaçado'],
%FilmesMaria = ['Monty Python: O Sentido da Vida', 'Edukators'],
%FilmesEmComum = 'Abril despedaçado'.

%%%%%%%%(u) Retornar a lista de tı́tulos dos filmes nunca alugados nem por ’Bob’ e nem por ’Maria’.

%%FilmesEmComum sao as respostas!!!!!!

%?- findall(Filme,(filme(IdFilme,Filme,_,_,_),dvd(IdDvd,IdFilme,_),locacao(IdCliente,IdDvd,_),cliente(
%IdCliente,Nome,_),Nome=='Bob'),FilmesBob), 
%findall(Filme,(filme(IdFilme,Filme,_,_,_),dvd(IdDvd,IdFilme,_),locacao(IdCliente,IdDvd,_),cliente(
%IdCliente,Nome,_),Nome=='Maria'),FilmesMaria), 
%findall(Filme,filme(_,Filme,_,_,_),Filmes), 
%sort(Filmes,FilmesOrd), 
%(member(FilmesEmComum,FilmesOrd),not(member(FilmesEmComum,FilmesBob)),not(member(FilmesEmComum,FilmesMaria))).

%FilmesBob = ['Monty Python: O Sentido da Vida', 'Monty Python: O Sentido da Vida', 'Edukators', 'Abril despedaçado'],
%FilmesMaria = ['Monty Python: O Sentido da Vida', 'Edukators'],
%Filmes = ['Monty Python: O Sentido da Vida', 'Edukators', 'Lavoura Arcaica', 'Lisbela e o Prisioneira', 'Abril despedaçado', 'Diários de
%motocicleta'],
%FilmesOrd = ['Abril despedaçado', 'Diários de motocicleta', 'Edukators', 'Lavoura Arcaica', 'Lisbela e o Prisioneira', 'Monty Python: O 
%Sentido da Vida'],
%FilmesEmComum = 'Diários de motocicleta' ;
%FilmesBob = ['Monty Python: O Sentido da Vida', 'Monty Python: O Sentido da Vida', 'Edukators', 'Abril despedaçado'],
%FilmesMaria = ['Monty Python: O Sentido da Vida', 'Edukators'],
%Filmes = ['Monty Python: O Sentido da Vida', 'Edukators', 'Lavoura Arcaica', 'Lisbela e o Prisioneira', 'Abril despedaçado', 'Diários de
%motocicleta'],
%FilmesOrd = ['Abril despedaçado', 'Diários de motocicleta', 'Edukators', 'Lavoura Arcaica', 'Lisbela e o Prisioneira', 'Monty Python: O
%Sentido da Vida'],
%FilmesEmComum = 'Lavoura Arcaica' ;
%FilmesBob = ['Monty Python: O Sentido da Vida', 'Monty Python: O Sentido da Vida', 'Edukators', 'Abril despedaçado'],
%FilmesMaria = ['Monty Python: O Sentido da Vida', 'Edukators'],
%Filmes = ['Monty Python: O Sentido da Vida', 'Edukators', 'Lavoura Arcaica', 'Lisbela e o Prisioneira', 'Abril despedaçado', 'Diários de
%motocicleta'],
%FilmesOrd = ['Abril despedaçado', 'Diários de motocicleta', 'Edukators', 'Lavoura Arcaica', 'Lisbela e o Prisioneira', 'Monty Python: O 
%Sentido da Vida'],
%FilmesEmComum = 'Lisbela e o Prisioneira' ;
%false.