%Crie uma estrutura de dados para pontos 2D.

ponto(X,Y).

%A: Crie uma regra distancia, a qual recebe dois pontos como parâmetro de deve retornar a distância
%entre eles.

distancia(ponto(X1,Y1), ponto(X2,Y2), Distancia) :-
	Soma is ((X2-X1)^2) + ((Y2-Y1)^2),
	Distancia is sqrt(Soma).

%?- distancia(ponto(1,1),ponto(1,4),Distancia).
%Distancia = 3.0.

%?- distancia(ponto(2,1),ponto(15,32),Distancia).
%Distancia = 33.61547262794322.

%B: Crie uma regra colineares, a qual recebe três pontos como parâmetro e é verdade se eles são
%colineares. DICA: os três pontos serão colineares se o determinante de suas coordenadas for igual a
%0.

colineares(ponto(X1,Y1),ponto(X2,Y2),ponto(X3,Y3)) :-
	((X1*Y2)+(Y1*X3)+(X2*Y3))-((Y1*X2)+(X1*Y3)+(Y2*X3)) =:= 0.

%?- colineares(ponto(0,5),ponto(1,3),ponto(2,1)).
%true.

%?- colineares(ponto(1,1),ponto(2,2),ponto(3,7)).
%false.

%C: Crie uma regra formaTriangulo, a qual recebe três pontos como parâmetro e é verdade se eles podem
%ser usados para formar um triângulo.

formaTriangulo(ponto(X1,Y1),ponto(X2,Y2),ponto(X3,Y3)) :-
	not(colineares(ponto(X1,Y1),ponto(X2,Y2),ponto(X3,Y3))), !,
	(not(X1 == X2);not(Y1 == Y2)), !,
	(not(X1 == X3);not(Y1 == Y3)), !,
	(not(X2 == X3);not(Y2 == Y3)).

%?- formaTriangulo(ponto(1,1),ponto(5,3),ponto(8,1)).
%true .

%?- formaTriangulo(ponto(1,1),ponto(2,2),ponto(3,3)).
%false.