%Motifique o arquivo arvore.pl (disponı́vel no Moodle) de forma a adicionar novas operações a nossa
%árvore. Assuma que nossa árvore não é uma árvore binária de busca.

no(52,
	no(32,
		no(12,nil,nil),
		no(35,nil,nil)),
	no(56,
		no(55,
			no(55,nil,nil),
			nil),
		no(64,nil,nil))).

soma(nil, 0).
soma(no(N, ArvE, ArvD),Soma) :-
	soma(ArvE,SomaEsq),
	soma(ArvD,SomaDir),
	Soma is N + SomaEsq + SomaDir.

membro(N, no(N, _, _)) :- !.
membro(N, no(_, ArvE, ArvD)) :-
	membro(N, ArvE), !;
	membro(N, ArvD).

min(X,Y,X) :- X =< Y, !.
min(X,Y,Y).

minimo(nil,1000). %INF = 1000
minimo(no(N, ArvE, ArvD),Min) :-
	minimo(ArvE, MinE),
	minimo(ArvD, MinD),
	min(MinE, MinD, MinED),
	min(N, MinED, Min).

caminho(Y, no(Y, _, _), [Y]) :- !.
caminho(Y, no(K, ArvE, ArvD), [K|Caminho]) :-
	caminho(Y, ArvE, Caminho), !;
	caminho(Y, ArvD, Caminho).

%A: Crie uma regra que receba um valor X e uma árvore e retorne a quantidade de ocorrências dele na
%árvore.

%B: Crie uma regra que receba um valor X e uma árvore e retorne a quantidade de elementos maiores que
%ele na árvore.

%C: Crie uma regra que receba uma árvore e retorne a média dos elementos na árvore.

%D: Crie uma regra que receba uma árvore e retorne a quantidade de elementos na árvore.

%E: Crie uma regra para transformar uma árvore em uma lista. Esta lista deve simplesmente conter todos
%os elementos da árvore.

%F: Crie uma regra que receba um valor X e uma árvore e seja verdade quando o valor X não está presente
%na árvore.

%G: Crie uma regra que receba uma árvore e retorne a lista de números visitados considerando o percor-
%rimento em pós-ordem.

%H: Crie uma regra que receba uma árvore e retorne a lista de números visitados considerando o percor-
%rimento em pré-ordem.

%I: Crie uma regra que receba uma árvore e retorne a lista de números visitados considerando o percorri-
%mento em em-ordem.

%J: Crie uma regra que receba uma árvore e retorne a quantidade de folhas nela existentes.

%K: Crie uma regra que receba uma árvore e retorne uma lista com todos os elementos encontrados nas
%folhas da árvore.

%L: Crie uma regra que receba duas árvore e seja verdade quando as duas árvores forem iguais. Duas
%árvores são iguais se elas possuem os mesmos elementos, dispostos da mesma forma.

%M: Crie uma regra que receba uma árvore e retorne a altura da árvore. Uma árvore com apenas o nó
%raiz possui altura 0. Assuma que não há nenhuma árvore com nenhum nó.

%N: Crie uma regra que receba uma árvore e dois elementos, X e Y, (sempre presentes na árvore) e retorne
%o caminho (uma lista de elementos) entre os dois elementos, ou seja, que nós devem ser visitados
%para ir de um nó com o elemento X até um nó com o elemento Y.

%O: Faça consultas para demonstrar o uso de cada item e inclua (em comentário) no final do seu arquivo
%.pl.