%OK

%Crie uma regra distancia3D(ponto(X1,Y1,Z1),ponto(X2,Y2,Z2), Dist) que dados dois pontos no espaço
%3D, (x1, y1, z1) e (x2, y2, z2), compute a distância (Dist) entre eles.

distancia3D(ponto(X1,Y1,Z1),ponto(X2,Y2,Z2), Dist) :-
	Quadrado1 is ((X2-X1)*(X2-X1)) ,
	Quadrado2 is ((Y2-Y1)*(Y2-Y1)) ,
	Quadrado3 is ((Z2-Z1)*(Z2-Z1)) ,
	Dist is sqrt(Quadrado1+Quadrado2+Quadrado3) .

%?- distancia3D(ponto(4,-8,-9),ponto(2,-3,-5),X).
%X = 6.708203932499369.