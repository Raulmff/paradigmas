%Crie uma regra coprimos(X,Y) que receba dois números X e Y e indique se eles são coprimos. Dois
%números são ditos coprimos se o máximo divisor comum entre eles é 1.

mdc(0,Y,Y) :- ! .

mdc(X,Y,Resultado) :-
	X < Y , ! ,
	X1 is Y mod X ,
	mdc(X1,X,Resultado) .

mdc(X,Y,Resultado) :-
	X1 is X mod Y ,
	mdc(X1,Y,Resultado), ! .

coprimos(X,Y) :- 	mdc(X,Y,MDC) , 
					MDC =:= 1 .

%?- coprimos(20,21).
%true.

%?- coprimos(2,4).
%false.					