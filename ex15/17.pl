%Crie uma regra mmc(X,Y,Resultado) que receba dois números X e Y e retorne o mı́nimo múltiplo comum
%(DICA: use a regra do máximo divisor comum já criada).

mdc(0,Y,Y) :- ! .

mdc(X,Y,Resultado) :-
	X < Y , ! ,
	X1 is Y mod X ,
	mdc(X1,X,Resultado) .

mdc(X,Y,Resultado) :-
	X1 is X mod Y ,
	mdc(X1,Y,Resultado), ! .

mmc(X,Y,Resultado) :- 	mdc(X,Y,MDC) ,
						Resultado is ((X*Y)/MDC) .

%?- mmc(420,66,X).
%X = 4620.

%?- mmc(5,47,X).
%X = 235.