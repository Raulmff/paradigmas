%Crie uma regra primo(N) que receba um número N e retorne se o mesmo é primo.

divisivel(N,K) :- mod(N,K) =:= 0 .

primo(2).
primo(N) :-	(N mod 2) =\= 0 ,	
			primo(N, 3) .

primo(N, F) :- F >= N .

primo(N, F) :- 	F < N,
  				(N mod F) =\= 0,
  				F1 is F + 1,
				primo(N, F1) .

%?- primo(5).
%true .

%?- primo(12).
%false.

%?- primo(29).
%true .

%?- primo(37).
%true .				