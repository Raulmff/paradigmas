%OK

genitor(pam, bob).
genitor(tom, bob).
genitor(tom, liz).

genitor(bob, ana).
genitor(bob, pat).

genitor(liz, bill).

genitor(pat, jim).

mulher(pam).
mulher(liz).
mulher(pat).
mulher(ana).
homem(tom).
homem(bob).
homem(jim).
homem(bill).

pai(X,Y) :- genitor(X,Y), homem(X).
mae(X,Y) :- genitor(X,Y), mulher(X).

avo(AvoX, X) :- genitor(GenitorX, X), genitor(AvoX, GenitorX), homem(AvoX).
avoh(AvohX, X) :- genitor(GenitorX, X), genitor(AvohX, GenitorX), mulher(AvohX).
irmao(X,Y) :- genitor(PaiAmbos, X), genitor(PaiAmbos, Y), X \== Y, homem(X).
irma(X,Y) :- genitor(PaiAmbos, X), genitor(PaiAmbos, Y), X \== Y, mulher(X).
irmaos(X,Y) :- (irmao(X,Y); irma(X,Y)), X \== Y.

ascendente(X,Y) :- genitor(X,Y). %recursão - caso base
ascendente(X,Y) :- genitor(X, Z), ascendente(Z, Y). %recursão - passo recursivo

%Modifique o arquivo familia.pl de forma a incluir as seguintes regras:

%tio(X,Y), onde X é o tio de Y.
tio(X,Y) :- genitor(PaiOuMae,Y) , 
			irmao(X,PaiOuMae) .

%?- tio(X,bill).
%X = bob ;
%false.

%tia(X,Y), onde X é a tia de Y.
tia(X,Y) :- genitor(PaiOuMae,Y) , 
			irma(X,PaiOuMae) .

%?- tia(X,ana).
%X = liz ;
%false.

%primo(X,Y), onde X é o primo de Y.
primo(X,Y) :- 	genitor(GenitorDeX,X) , 
				genitor(GenitorDeY,Y) , 
				irmaos(GenitorDeX,GenitorDeY) , 
				homem(X) .

%?- primo(X,ana).
%X = bill ;
%false.

%prima(X,Y), onde X é a prima de Y.
prima(X,Y) :- 	genitor(GenitorDeX,X) , 
				genitor(GenitorDeY,Y) , 
				irmaos(GenitorDeX,GenitorDeY) , 
				mulher(X) .

%?- prima(X,bill).
%X = ana ;
%X = pat ;
%false.

%primos(X,Y), onde X é primo ou prima de Y.
primos(X,Y) :- 	primo(X,Y) ;
				prima(X,Y) ;
				X \== Y .

%?- primos(X,Y).
%X = bill, Y = ana ; X = bill, Y = pat ; X = ana, Y = bill ; X = pat, Y = bill ;
%true.

%bisavo(X,Y), onde X é o bisavô de Y.
bisavo(X,Y) :- 	genitor(GenitorDeY,Y) ,
				avo(X,GenitorDeY) .

%?- bisavo(X,jim).
%X = tom.

%bisavoh(X,Y), onde X é a bisavó de Y.
bisavoh(X,Y) :- genitor(GenitorDeY,Y) ,
				avoh(X,GenitorDeY) .

%?- bisavoh(X,jim).
%X = pam.

%descendente(X,Y), onde X é descendente de Y.
descendente(X,Y) :- ascendente(Y,X) .

%?- descendente(X,bob).
%X = ana ;
%X = pat ;
%X = jim ;
%false.

%feliz(X), onde X é feliz se possui filhos.
feliz(X) :- descendente(DescendenteDeX,X) ,
			DescendenteDeX \== X .

%?- feliz(liz).
%true .

%?- feliz(jim).
%false.

%Crie outra regra a sua escolha, descreva seu funcionamento e exemplifique seu uso por meio de uma
%É um filho feliz se tem irmao(s)
filhoFeliz(X) :- irmaos(X,Z) .
