%OK

%Crie uma regra maior(A,B,C,X) que receba 3 valores numéricos (A, B, C) e retorne o maior deles (X).

maior(A,B,C,A) :- A > B , A > C .
maior(A,B,C,B) :- B > A , B > C .
maior(A,B,C,C) :- C > A , C > B .

%?- maior(1,2,3,X).
%X = 3.

%?- maior(1,5,3,X).
%X = 5 .

%?- maior(9,5,3,X).
%X = 9 .