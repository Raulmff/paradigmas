%OK

%Crie uma regra areaTriangulo(B,A,Area) que receba a base e a altura de um triângulo e calcule a área
%do mesmo.

areaTriangulo(B,A,Area) :- Area is ((B*A)/2) .

%?- areaTriangulo(10,3,X).
%X = 15.

%?- areaTriangulo(3,15,X).
%X = 22.5.