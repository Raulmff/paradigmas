%OK

%Crie uma regra triangulo(X,Y,Z) que receba três valores X, Y e Z e indique se havendo varetas com
%esses valores em comprimento pode-se construir um triângulo. Exemplo, com varetas de comprimento 4,
%8 e 9 posso construir um triângulo, porém com varetas de comprimento 10, 5 e 4 não posso construir um
%triângulo.

triangulo(X,Y,Z) :- X > abs(Y-Z) ,
					X < Y+Z ,
					Y > abs(X-Z) ,
					Y < X+Z ,
					Z > abs(X-Y) ,
					Z < X+Y .

%?- triangulo(4,8,9).
%true.

%?- triangulo(10,5,4).
%false.

%?- triangulo(10,14,8).
%true.