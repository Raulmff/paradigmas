%Crie uma regra totienteEuler(N,K) que receba um número n e retorne (em K) o resultado da função
%totiente de Euler (φ(n)). A função totiente de Euler é dada pelo número de inteiros positivos r a partir
%de 1 e menores que n, ou seja 1 <= r < n, que são coprimos de n. Por exemplo, se n = 10, então os
%coprimos de 10 de 1 até 10-1 são {1, 3, 7, 9} e a função deve retornar φ(n) = 4. Além disso, φ(1) = 1.

mdc(0,Y,Y) :- ! .

mdc(X,Y,Resultado) :-
	X < Y , ! ,
	X1 is Y mod X ,
	mdc(X1,X,Resultado) .

mdc(X,Y,Resultado) :-
	X1 is X mod Y ,
	mdc(X1,Y,Resultado), ! .

coprimos(X,Y) :- 	mdc(X,Y,MDC) , 
					MDC =:= 1 .

totienteEuler(1, 1).
totienteEuler(N, K) :- 	N1 is N - 1,
  						totAux(N, N1, K).

totAux(_, 0, 0).
totAux(N, R, K) :- 	coprimos(N, R), 	
					R1 is R - 1, 	
					totAux(N, R1, P1), 	
					K is P1 + 1.

totAux(N, R, K) :- 	\+ coprimos(N, R), 
					R1 is R - 1,  
					totAux(N, R1, K).						

%?- totienteEuler(10,X).
%X = 4 .				