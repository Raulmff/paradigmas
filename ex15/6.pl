%OK

%Crie uma regra potencia(X,Y,Resultado), onde Resultado é X Y .

potencia(_,0,1) .
potencia(X,Y,Resultado) :- 	Y>0 , 
					M is Y-1 ,
					potencia(X,M,R) ,
					Resultado is X*R .

%?- potencia(2,3,X).
%X = 8 .

%?- potencia(2,5,X).
%X = 32 .

%?- potencia(5,2,X).
%X = 25 .
 
