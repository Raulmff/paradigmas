%OK

%Crie uma regra absoluto(N,X) que receba um número N , negativo ou positivo, e retorne seu valor absoluto
%X.

absoluto(N,X) :- X is abs(N) .

%?- absoluto(1,X).
%X = 1.

%?- absoluto(-1,X).
%X = 1.