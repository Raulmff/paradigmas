%Crie uma regra aprovado(A,B,C) que receba três notas de um aluno (A, B, C), calcule a média e indique
%se o aluno foi aprovado ou reprovado. Para um aluno ser aprovado, ele deve possuir nota igual ou superior
%a 6.

aprovado(A,B,C) :- 	Media is ((A+B+C)/3) ,
					Media >= 6 .

%?- aprovado(5,5,5).
%false.

%?- aprovado(7,6,5).
%true.

%?- aprovado(7,6,4).
%false.

%?- aprovado(7,9,10).
%true.
