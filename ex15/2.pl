%OK

%Crie um novo programa Prolog para o seguinte cenário.

%• Crie fatos para representar as sentenças abaixo:

%– um estudante estuda uma disciplina, sendo que uma disciplina é formada por 3 letras e 4 dı́gitos
%(ex: ine5416).
estuda(joao,ine5416) .
estuda(joana,ine5416) .
estuda(pedro,ine5415) .
estuda(pedrona,ine5415) .
estuda(paulo,ine5413) .
estuda(paula,ine5413) .
estuda(paulona,ine5413) .

%– um professor leciona uma disciplina.
leciona(maicon,ine5416) .
leciona(maicon,ine5413) .
leciona(cancian,ine5415) .

%• Crie regras que representem as sentenças abaixo:
%– Um professor ensina um aluno se o professor leciona uma disciplina em que o aluno estuda.
ensina(X,Y) :- 	leciona(X,MesmaDisciplina) ,
				estuda(Y,MesmaDisciplina) .

%?- ensina(maicon,X).
%X = joao ;
%X = joana ;
%X = paulo.

%– Dois estudantes são colegas de classe se estudam a mesma disciplina e não são o mesmo aluno.
colegas(X,Y) :- 	estuda(X,MesmaDisciplina) ,
					estuda(Y,MesmaDisciplina) ,
					X \== Y .

%?- colegas(X,joao).
%X = joana ;
%false.

%– Crie consultas para as seguintes perguntas abaixo:

%∗ Quais são todas as disciplinas lecionadas pelo professor x? (substitua x por algum nome de
%professor no seu programa)

%?- leciona(maicon,X).
%X = ine5416 ;
%X = ine5413.

%∗ Quais são todos os alunos do professor x? (substitua x por algum nome de professor no seu
%programa)

%?- ensina(cancian,X).
%X = pedro ;
%X = pedrona.

%∗ Quais são todos os amigos do estudante y? (substitua y por algum nome de aluno no seu
%programa)

%?- colegas(paulo,X).
%X = paula ;
%X = paulona.

%∗ a e b são amigos? (substitua a e b por nomes de alunos no seu programa)

%?- colegas(joao,joana).
%true.

%• Crie outra regra a sua escolha, descreva seu funcionamento e exemplifique seu uso por meio de uma
%consulta.
%Encontra se um professor leciona mais de uma disciplina
lecionaMaisDeUma(X) :- 	leciona(X,Disciplina1) ,
						leciona(X,Disciplina2) ,
						Disciplina1 \== Disciplina2 .

%?- lecionaMaisDeUma(maicon).
%true .

%?- lecionaMaisDeUma(cancian).
%false.						