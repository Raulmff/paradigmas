%OK

%Crie uma regra eqSegundoGrau(A,B,C,ValorX) que resolva uma equação de segundo grau da forma
%ax 2 + bx + c utilizando a fórmula de Bhaskara.

eqSegundoGrau(A,B,C,ValorX):-
	D is ((B*B)-(4*A*C)) ,
	D >= 0 ,
	ValorX is ((-B+sqrt(D))/2*A) .

%?- eqSegundoGrau(1,2,3,X).
%false.

%?- eqSegundoGrau(1,-5,6,X).
%X = 3.0.