%OK

%Crie uma regra xor(X,Y) que receba dois valores booleanos X e Y e indique se a operação X xor Y é
%verdadeira. Construa a regra apenas usando os operadores , (and), ; (or) e not.

xor(X,Y) :- X \== Y .

%?- xor(true,false).
%true.

%?- xor(false,false).
%false.

%?- xor(true,true).
%false.

%?- xor(false,true).
%true.