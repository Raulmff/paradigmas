%OK

%Crie uma regra operacao(Op,X,Y,Resultado) que receba três parâmetros Operador, X e Y, e retorne o
%resultado da operação matemática X Operador Y . Os operadores possı́veis de informar são +, -, *, /.

operacao('+',X,Y,Resultado) :- Resultado is X+Y .
operacao('-',X,Y,Resultado) :- Resultado is X-Y .
operacao('*',X,Y,Resultado) :- Resultado is X*Y .
operacao('/',X,Y,Resultado) :- 	Y =\= 0 ,
 								Resultado is X/Y .

%?- operacao(+,1,2,X).
%X = 3.

%?- operacao(-,1,2,X).
%X = -1.

%?- operacao(*,1,2,X).
%X = 2.

%?- operacao(/,1,2,X).
%X = 0.5.