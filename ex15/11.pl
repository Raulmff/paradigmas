%OK

%Crie uma regra fib(N,K) que compute o N-ésimo número de Fibonacci K.

fib(1,1) .
fib(2,1) .
fib(N,K) :- N1 is N-1 ,
			N2 is N-2 ,
			fib(N1,A) , 
			fib(N2,B) ,
			K is A+B .

%?- fib(3,X).
%X = 2 .

%?- fib(4,X).
%X = 3 .

%?- fib(5,X).
%X = 5 .

%?- fib(6,X).
%X = 8 .

%?- fib(7,X).
%X = 13 .
