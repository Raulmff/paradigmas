%OK

%Crie uma regra mdc(X,Y,Resultado) que receba dois números X e Y e retorne o máximo divisor comum
%(DICA: pesquise sobre o Algoritmo de Euclides).

mdc(0,Y,Y) :- ! .

mdc(X,Y,Resultado) :-
	X < Y , ! ,
	X1 is Y mod X ,
	mdc(X1,X,Resultado) .

mdc(X,Y,Resultado) :-
	X1 is X mod Y ,
	mdc(X1,Y,Resultado) ,! .

%?- mdc(16,24,X).
%X = 8 .

%?- mdc(13,5,X).
%X = 1 .

%?- mdc(420,66,X).
%X = 6.