%OK

%Crie uma regra divisivel(N,K) para dizer se um número N é divisı́vel por K.

divisivel(N,K) :- mod(N,K) =:= 0 .

%?- divisivel(4,2).
%true.

%?- divisivel(2,4).
%false.

%?- divisivel(9,3).
%true.

%?- divisivel(27,3).
%true.

%?- divisivel(27,4).
%false.

%?- divisivel(27,5).
%false.