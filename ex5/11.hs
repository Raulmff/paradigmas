-- 11. Crie uma função com assinatura primeiros :: Int -> [t] -> [t], a qual recebe um número de ele-
-- mentos, uma lista, e retorna uma lista. Esta função deve retornar uma lista com os n primeiros elementos
-- informados no primeiro parâmetro. Não utilize nenhuma função pronta to Haskell para esta tarefa.

--OK

primeiros :: Int -> [t] -> [t]
primeiros 0 _ = []
primeiros n [] = []
primeiros n (a:b) = [a] ++ (primeiros (n-1) b)

main = do
	print (primeiros 10 [1,2,3,4,5,6,7,8])
