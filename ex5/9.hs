-- Crie uma função com assinatura mapear :: (t -> y) -> [t] -> [y], a qual recebe uma função, uma
-- lista e retorna uma lista. Esta função mapear fará o mesmo que a função map, ou seja, aplicar a função
-- recebida como parâmetro sobre cada elemento da lista e retornar a lista resultante. Não utilize map ou
-- filter para esta tarefa.

--OK

mapear :: (t -> y) -> [t] -> [y]
mapear f [] = []
mapear f (a:b) = [(f a)] ++ (mapear f b)

funcao :: Int -> Float
funcao x = ((fromIntegral x :: Float)/2)

main = do
	print ( mapear funcao [1,2,3,4,5,6,7,8] )
