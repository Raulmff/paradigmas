--Crie uma função com assinatura media :: [Int] -> Float, a qual recebe uma lista de Int e retorna
--a média de todos os elementos da lista. Retorne 0 caso a lista for vazia. Não utilize nenhuma função
--pronta do Haskell para realizar esta tarefa. DICA: utilize a função fromIntegral para converter um tipo
--inteiro para um tipo compatı́vel com o operador de divisão /

--OK

comprimento :: [Int] -> Int
comprimento [] = 0
comprimento (_:b) = 1 + (comprimento b)

media :: [Int] -> Int -> Int -> Float
media [] size soma = (fromIntegral soma :: Float) / (fromIntegral size :: Float)
media (a:b) size soma = (media b size (soma+a))

soma :: Int -> [Int] -> Int
soma a (b:c) = a+b

main = do
	let comp = (comprimento [10,67,88,42])
	print (media [10,67,88,42] comp 0)
