-- Crie uma função com assinatura inverte :: [t] -> [t], a qual recebe uma lista como parâmetro e
-- deve retornar a mesma invertida. Não utilize nenhuma função pronta do Haskell para realizar esta tarefa.

--OK

inverte :: [t] -> [t]
inverte [] = []
inverte (a:b) = inverte b ++ [a]

main = do

	print (inverte [1,2,3,4,5,6])
