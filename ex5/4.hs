--Crie uma função com assinatura diferencaMaiorMenor :: [Int] -> Int, a qual recebe uma lista de
--Int e retorna a diferença entre o maior e o menor elemento da lista. Retorne 0 caso a lista for vazia. Não
--utilize nenhuma função pronta do Haskell para realizar esta tarefa.

--OK

diferencaMaiorMenor :: [Int] -> Int
diferencaMaiorMenor [] = 0
diferencaMaiorMenor (a:b) = ( (maior (a:b)) - (menor (a:b)) )
	
menor :: [Int] -> Int
menor [] = 0
menor (a:b) = (addMenor a b)

addMenor :: Int -> [Int] -> Int
addMenor a [] = a
addMenor a (b:c) | (a <= b) = (addMenor a c)
			| otherwise = (addMenor b c)

maior :: [Int] -> Int
maior [] = 0
maior (a:b) = (addMaior a b)

addMaior :: Int -> [Int] -> Int
addMaior a [] = a
addMaior a (b:c) | (a >= b) = (addMaior a c)
			| otherwise = (addMaior b c)

main = do
	print (diferencaMaiorMenor [5,8,7,9,20,10])