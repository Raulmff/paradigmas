-- 10. Crie uma função com assinatura filtrar :: (t -> Bool) -> [t] -> [t], a qual recebe uma função,
-- uma lista e retorna uma nova lista. Esta função aplica a função recebida como parâmetro sobre cada
-- elemento da lista e caso o retorno da função for verdadeiro, então o elemento fará parte da nova lista, caso
-- contrário não. Para esta tarefa, utilize o conceito de list comprehension.

-- OK

filtrar :: (t -> Bool) -> [t] -> [t]
filtrar f [] = []
filtrar f (a:b) = 
	if ((f a) == True)
		then [a] ++ (filtrar f b)
		else (filtrar f b)

ehPar :: Int -> Bool
ehPar n =
	if ((n `mod` 2) == 0)
		then True
		else False

main = do
	print (filtrar ehPar [1,2,3,4,5,6,7,8,9,0])