-- Crie uma função com assinatura menor :: [Int] -> Int, a qual recebe uma lista de Int e retorna o
-- menor elemento da lista. Retorne 0 caso a lista for vazia. Não utilize nenhuma função pronta do Haskell
-- para realizar esta tarefa.

--OK

menor :: [Int] -> Int
menor [] = 0
menor (a:b) = (add a b)

add :: Int -> [Int] -> Int
add a [] = a
add a (b:c) | (a <= b) = (add a c)
			| otherwise = (add b c)

main = do
	print (menor [5,8,2,9,1,3])