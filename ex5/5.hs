--Crie uma função com assinatura busca :: [Int] -> Int -> Bool, a qual recebe uma lista de Int e um
--Int e retorna se o elemento passado como parâmetro encontra-se na lista ou não. Não utilize nenhuma
--função pronta do Haskell para realizar esta tarefa.

--OK

busca :: [Int] -> Int -> Bool
busca [] c = False
busca (a:b) c 
	| (a == c) = True
	| otherwise = (busca b c)

main = do
	print (busca [5,8,2,9,1,3] 9) 	