-- 7. Motifique o arquivo alunos.hs (disponı́vel no Moodle) de forma a adicionar novas funções:,

--FINALIZAR

alunos :: [(Int, String, Float)]
alunos = [(1, "Ana", 3.4), (2, "Bob", 6.7), (3, "Tom", 7.6)]

getNome :: (Int, String, Float) -> String
getNome (a,b,c) = b

-- 11. Crie uma função com assinatura primeiros :: Int -> [t] -> [t], a qual recebe um número de ele-
-- mentos, uma lista, e retorna uma lista. Esta função deve retornar uma lista com os n primeiros elementos
-- informados no primeiro parâmetro. Não utilize nenhuma função pronta to Haskell para esta tarefa.
getPrimeiroAluno :: [(Int, String, Float)] -> (Int, String, Float)
getPrimeiroAluno (a:_) = a

-- A: Crie uma função com a seguinte assinatura: aprovados :: [(Int, String, Float)] -> [String],
-- a qual recebe uma lista de alunos e retorna uma lista com o nome dos alunos aprovados. Um aluno
-- está aprovado se a sua média é maior ou igual a 6. Utilize map e filter para resolver esta questão.
aprovados :: [(Int, String, Float)] -> [String]
aprovados l = (map getNome (filter mediaAcima l))

mediaAcima :: (Int, String, Float) -> Bool
mediaAcima (_,_,m) 
	| (m >= 6) = True
	| otherwise = False

-- B: Crie uma função com a seguinte assinatura: aprovados2 :: [(Int, String, Float)] -> [String],
-- a qual recebe uma lista de alunos e retorna uma lista com o nome dos alunos aprovados. Um aluno
-- está aprovado se a sua média é maior ou igual a 6. Não utilize map e filter para resolver esta
-- questão. Utilize o conceito de list comprehension.
aprovados2 :: [(Int, String, Float)] -> [String]
aprovados2 [] = []
aprovados2 (a:b) = 
	if (mediaAcima a) 
		then
			[(getNome a)] ++ (aprovados2 b)
		else 
			(aprovados2 b)

--FINALIZAR

-- C: Utilize (e modifique, se necessário) a função gerarPares vista em aula e disponı́vel no arquivo
-- alunos.hs para formar duplas de alunos. Note que um aluno não pode fazer dupla consigo mesmo.
gerarPares :: [t] -> [u] -> [(t,u)]
gerarPares l1 l2 = [(a, b) | a <- l1, b <- l2]            

obterNomes :: [(Int, String, Float)] -> [String]
obterNomes [] = []
obterNomes (a:b) = [(getNome a)] ++ (obterNomes b)


main = do
    --print (getPrimeiroAluno alunos)
    --print (aprovados alunos)
    --print (aprovados2 alunos)
    --print (obterNomes alunos)
    let nomes = obterNomes alunos
    print (gerarPares nomes nomes)