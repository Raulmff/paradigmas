-- Crie uma função com assinatura ocorrencias :: [Int] -> Int -> Int, a qual recebe uma lista de
-- Int e um Int e retorna o número de vezes em que o elemento está presente na lista. Não utilize nenhuma
-- função pronta do Haskell para realizar esta tarefa.

--OK

ocorrencias :: [Int] -> Int -> Int
ocorrencias [] _ = 0
ocorrencias (a:b) c
	| (a == c) = 1 + (ocorrencias b c)
	| otherwise = (ocorrencias b c)

main = do
	print (ocorrencias [9,8,2,9,1,9] 9)
