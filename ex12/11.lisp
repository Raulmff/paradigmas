; Crie uma função com assinatura primeiros, a qual recebe um número de elementos, uma lista, e retorna
; uma lista. Esta função deve retornar uma lista com os n primeiros elementos informados no primeiro
; parâmetro. Não utilize nenhuma função pronta to LISP para esta tarefa.