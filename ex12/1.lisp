; Crie uma função com assinatura soma, a qual recebe uma lista de inteiros e retorna a soma de todos os
; elementos da lista. Retorne 0 caso a lista for vazia. Não utilize nenhuma função pronta to LISP para
; esta tarefa.

;OK

(defun soma (lista)
	(if (null lista)
		0
		(+ (first lista) (soma (rest lista)))
	)
)

(defun main()
	(write-line (write-to-string (soma '(1 2 3 4))))
)

(main)