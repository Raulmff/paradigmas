; Crie uma função com assinatura apagar, a qual recebe, um número de elementos (n), uma lista, e retorna
; uma lista. Esta função deve remover da lista os n primeiros elementos fornecidos como parâmetro. Por
; exemplo, a chamada (apagar 3 ’(1 2 3 4 5)) deve retornar (4 5). Não utilize nenhuma função
; pronta to LISP para esta tarefa.