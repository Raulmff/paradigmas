; Crie uma função com assinatura menor, a qual recebe uma lista de inteiros e retorna o menor elemento
; da lista. Retorne 0 caso a lista for vazia. Não utilize nenhuma função pronta to LISP para esta tarefa.

;OK

(defun menor (lista)
	(if (null lista)
		0
		(menor2 lista 123456789)
	)
)

(defun menor2 (lista menorLista)
	(if (null lista)
		menorLista 
		(if ( < (first lista) menorLista)
			(menor2 (rest lista) (first lista))
			(menor2 (rest lista) menorLista)
		)
	)
)

(defun main()
	(write-line (write-to-string (menor '(1 2 3))))
	(write-line (write-to-string (menor '(10 5 9 3 14))))
)

(main)