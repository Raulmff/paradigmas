; Crie uma função com assinatura busca, a qual recebe uma lista de inteiros e um inteiro e retorna se o
; elemento passado como parâmetro encontra-se na lista ou não. Não utilize nenhuma função pronta do
; LISP para realizar esta tarefa.

;OK

(defun busca (lista elemento)
	(if (null lista)
		NIL
		(if ( = (first lista) elemento)
			T
			(busca (rest lista) elemento)
		)
	)
)

(defun main()
	(write-line (write-to-string (busca '(1 2 3) 2)))
	(write-line (write-to-string (busca '(10 5 9 3 14) 1)))
)

(main)