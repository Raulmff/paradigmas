; Crie uma função com assinatura diferencaMaiorMenor, a qual recebe uma lista de inteiros e retorna
; a diferença entre o maior e o menor elemento da lista. Retorne 0 caso a lista for vazia. Não utilize
; nenhuma função pronta to LISP para esta tarefa.

;OK

(defun menor (lista)
	(if (null lista)
		0
		(menor2 lista 123456789)
	)
)

(defun menor2 (lista menorLista)
	(if (null lista)
		menorLista 
		(if ( < (first lista) menorLista)
			(menor2 (rest lista) (first lista))
			(menor2 (rest lista) menorLista)
		)
	)
)

(defun maior (lista)
	(if (null lista)
		0
		(maior2 lista 0)
	)
)

(defun maior2 (lista maiorLista)
	(if (null lista)
		maiorLista 
		(if ( > (first lista) maiorLista)
			(maior2 (rest lista) (first lista))
			(maior2 (rest lista) maiorLista)
		)
	)
)

(defun diferencaMaiorMenor (lista) 
	( - (maior lista) (menor lista))
)

(defun main()
	(write-line (write-to-string (diferencaMaiorMenor '(1 2 3))))
	(write-line (write-to-string (diferencaMaiorMenor '(10 5 9 3 14))))
)

(main)