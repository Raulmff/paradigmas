; Crie uma função com assinatura inverte, a qual recebe uma lista como parâmetro e deve retornar a
; mesma invertida. Não utilize nenhuma função pronta do LISP para realizar esta tarefa.

;OK

(defun inverte (lista)
	(if (null lista)
		()
		(concatenate 'list (inverte (rest lista)) (cons (first lista) ()) )
	)
)

(defun main()
	; Lista começa em 0...
	(write-line (write-to-string (inverte '(1 2 3))))
	(write-line (write-to-string (inverte '(5 12 45 78 96 32))))
)

(main)