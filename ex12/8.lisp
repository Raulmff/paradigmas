; Crie uma função com assinatura fatia, a qual recebe uma lista de inteiros, um inteiro n e um inteiro m
; e retorna todos os elementos da lista a partir da posição n (inclusive) até a posição m (exceto o elemento
; da posição m). Não utilize nenhuma função pronta to LISP para esta tarefa.

;OK

(defun fatia (lista n m posAtual)
	(if (null lista)
		()
		(if (and (>= posAtual n) (< posAtual m))
			(cons (first lista) (fatia (rest lista) n m (+ 1 posAtual)))
			(fatia (rest lista) n m (+ 1 posAtual))
		)
	)
)

(defun main()
	; Lista começa em 0...
	(write-line (write-to-string (fatia '(1 2 3) 0 2 0)))
	(write-line (write-to-string (fatia '(10 5 9 3 14) 1 3 0)))
	(write-line (write-to-string (fatia '(10 5 9 5 14) 3 4 0)))
	(write-line (write-to-string (fatia '(10 5 9 5 14) 2 5 0)))
)

(main)