; Crie uma função com assinatura media, a qual recebe uma lista de inteiros e retorna a média de todos os
; elementos da lista. Retorne 0 caso a lista for vazia. Não utilize nenhuma função pronta to LISP para
; esta tarefa.

;OK

(defun comprimento (lista)
	(if (null lista)
		0
		(+ 1 (comprimento (cdr lista)))
	)
)

(defun soma (lista)
	(if (null lista)
		0
		(+ (first lista) (soma (rest lista)))
	)
)

(defun media (lista)
	(/ (soma lista) (comprimento lista))
)

(defun main()
	(write-line (write-to-string (media '(1 2 3))))
	(write-line (write-to-string (media '(2 5 9 10 14))))
)

(main)