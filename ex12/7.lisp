; Crie uma função com assinatura enesimo, a qual recebe uma lista de inteiros e um inteiro n e retorna o
; n-ésimo elemento na lista. Não utilize nenhuma função pronta to LISP para esta tarefa.

;OK

(defun enesimo (lista n)
	(if (null lista)
		0
		(if (= n 0)
			(first lista)
			(enesimo (rest lista) (- n 1))

		)
	)
)

(defun main()]
	; Lista começa em 0...
	(write-line (write-to-string (enesimo '(1 2 3) 1)))
	(write-line (write-to-string (enesimo '(10 5 9 3 14) 0)))
	(write-line (write-to-string (enesimo '(10 5 9 5 14) 3)))
	(write-line (write-to-string (enesimo '(10 5 9 5 14) 10)))
)

(main)