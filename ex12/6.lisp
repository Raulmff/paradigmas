; Crie uma função com assinatura ocorrencias, a qual recebe uma lista de inteiros e um inteiro e retorna o
; número de vezes em que o elemento está presente na lista. Não utilize nenhuma função pronta to LISP
; para esta tarefa.

;OK

(defun ocorrencias (lista elemento)
	(if (null lista)
		0
		(if ( = (first lista) elemento)
			( + 1 (ocorrencias (rest lista) elemento))
			( + 0 (ocorrencias (rest lista) elemento))
		)
	)
)

(defun main()
	(write-line (write-to-string (ocorrencias '(1 2 3) 2)))
	(write-line (write-to-string (ocorrencias '(10 5 9 3 14) 1)))
	(write-line (write-to-string (ocorrencias '(10 5 9 5 14) 5)))
)

(main)