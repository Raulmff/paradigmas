-- Crie uma função que receba um número n e retorne a função totiente de Euler (φ(n)). A função totiente
-- de Euler é dada pelo número de inteiros positivos r a partir de 1 e menores que n, ou seja 1 <= r < n,
-- que são coprimos de n. Por exemplo, se n = 10, então os coprimos de 10 de 1 até 10-1 são {1, 3, 7, 9} e
-- a função deve retornar φ(n) = 4. Leia n do teclado.

--OK

mdc :: Int -> Int -> Int
mdc a b | mod a b == 0 = b
        | mod b a == 0 = a
        | a > b = mdc b (a `mod` b)
        | a < b = mdc a (b `mod` a)

coprimos :: Int -> Int -> Bool
coprimos a b = if (mdc a b) == 1 
								then
									True
								else 
									False

euler :: Int -> Int -> Int
euler n 0 = 0
euler n a = if (coprimos n a) == True
								then
									1 + (euler n (a-1))
								else 
									(euler n (a-1))

main = do
	nString <- getLine
	let n = (read nString :: Int)

	print (euler n (n-1))
