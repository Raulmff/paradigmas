-- Crie uma função que receba dois números x e y e retorne o máximo divisor comum (DICA: pesquise sobre
-- o Algoritmo de Euclides). Leia x e y do teclado.

--OK

euclides :: Int -> Int -> Int
euclides x y = if (y == 0)
						then
							x
						else
							(euclides y (x `mod` y))


main = do
	xString <- getLine
	yString <- getLine

	let x = (read xString :: Int)
	let y = (read yString :: Int)

	print (euclides x y)


