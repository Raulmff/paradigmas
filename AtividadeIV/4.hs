-- Crie uma função que receba dois valores booleanos (x, y) retorne o resultado do “ou exclusivo” (XOR) sobre eles. 
-- A função apenas deve usar os operadores &&, || e not. Leia os valores x e y do teclado.

--OK

exclusivo :: Bool -> Bool -> Bool
exclusivo x y | x == True && y == False = True
        	  | x == False && y == True = True
        	  | otherwise = False
main = do
	xString <- getLine
	yString <- getLine
	let x = (read xString :: Bool)
	let y = (read yString :: Bool)
	print (exclusivo x y)