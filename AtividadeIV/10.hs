-- Crie uma função que receba 3 valores numéricos (a, b, c) e retorne o maior deles. Não utilize nenhuma
-- forma de ordenação. Leia os valores a, b, c do teclado.

--OK

maior :: Float -> Float -> Float -> Float
maior a b c 
	| (a > b) && (a > c) = a
	| (b > a) && (b > c) = b
	| otherwise = c

main = do
	aString <- getLine
	bString <- getLine
	cString <- getLine

	let a = (read aString :: Float)
	let b = (read bString :: Float)
	let c = (read cString :: Float)

	print (maior a b c)
