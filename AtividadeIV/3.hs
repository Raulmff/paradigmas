-- Crie uma função que receba a base e a altura de um triângulo e calcule a área do mesmo. Leia a base e a altura do teclado.

--OK

calculaArea :: Float -> Float -> Float
calculaArea base altura = (base * altura)/2

main = do
	baseString <- getLine
	alturaString <- getLine
	let base = (read baseString :: Float)
	let altura = (read alturaString :: Float)

	print (calculaArea base altura)