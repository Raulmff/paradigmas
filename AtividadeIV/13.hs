-- Crie uma função que receba dois números x e y e retorne o mı́nimo múltiplo comum (DICA: use a função
-- do máximo divisor comum já criada). Leia x e y do teclado.

--OK

mdc :: Int -> Int -> Int
mdc a b | mod a b == 0 = b
        | mod b a == 0 = a
        | a > b = mdc b (a `mod` b)
        | a < b = mdc a (b `mod` a)

mmc :: Int -> Int -> Int
mmc a b | a == 0 = 0
        | b == 0 = 0
        | a == b = a
		| otherwise = (div (a * b) (mdc a b) )

main = do
	xString <- getLine
	yString <- getLine

	let x = (read xString :: Int)
	let y = (read yString :: Int)

	print (mmc x y)
