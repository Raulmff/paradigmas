-- Crie uma função que receba três inteiros x, y e z e retorne se havendo varetas com esses valores em
-- comprimento pode-se construir um triângulo. Exemplo, com varetas de comprimento 4, 8 e 9 posso
-- construir um triângulo, porém com varetas de comprimento 10, 5 e 4 não posso construir um triângulo.
-- Leia x, y e z do teclado.

--OK

formaTriangulo :: Int -> Int -> Int -> Bool
formaTriangulo a b c 
	|(abs (b-c)) < a && (b+c) > a = True
	|(abs (a-c)) < b && (a+c) > b = True
	|(abs (a-b)) < c && (a+b) > c = True
	|otherwise = False

main = do
	aString <- getLine
	bString <- getLine
	cString <- getLine

	let a = (read aString :: Int)
	let b = (read bString :: Int)
	let c = (read cString :: Int)

	print (formaTriangulo a b c)