-- Crie uma função que receba dois números x e y e retorne x^y . Leia x e y do teclado.

--OK

funcao :: Float -> Float -> Float
funcao x y = x**y

main = do
	xString <- getLine
	yString <- getLine
	let x = (read xString :: Float)
	let y = (read yString :: Float)

	print (funcao x y)