-- Crie uma função que receba um número n e retorne se o mesmo é primo. Leia n do teclado.

--OK

ehPrimo :: Int -> Bool
ehPrimo 1 = False
ehPrimo 2 = True
ehPrimo n 	| (length [x | x <- [2 .. n-1], mod n x == 0]) > 0 = False
			| otherwise = True

main = do
	nString <- getLine
	let n = (read nString :: Int)

	print (ehPrimo n)
