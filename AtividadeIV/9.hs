-- Crie uma função que dados dois pontos no espaço 3D, (x1, y1, z1) e (x2, y2, z2), compute a distância
-- entre eles. Leia as posições dos pontos do teclado.

--OK

dis :: Float -> Float -> Float -> Float -> Float -> Float -> Float
dis x1 y1 z1 x2 y2 z2 = (sqrt ( (x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2 ) )


main = do
	print "Escreva os tres valores do primeiro ponto abaixo separados por ENTER"
	x1String <- getLine
	y1String <- getLine
	z1String <- getLine

	let x1 = (read x1String :: Float)
	let y1 = (read y1String :: Float)
	let z1 = (read z1String :: Float)

	print "Escreva os tres valores do segundo ponto abaixo separados por ENTER"
	x2String <- getLine
	y2String <- getLine
	z2String <- getLine

	let x2 = (read x2String :: Float)
	let y2 = (read y2String :: Float)
	let z2 = (read z2String :: Float)

	print (dis x1 y1 z1 x2 y2 z2)