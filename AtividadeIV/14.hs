-- Crie uma função que receba dois números x e y e determine se eles são coprimos. Dois números são ditos
-- coprimos se o máximo divisor comum entre eles é 1. Leia x e y do teclado.

--OK

mdc :: Int -> Int -> Int
mdc a b | mod a b == 0 = b
        | mod b a == 0 = a
        | a > b = mdc b (a `mod` b)
        | a < b = mdc a (b `mod` a)

coprimos :: Int -> Int -> Bool
coprimos a b = if (mdc a b) == 1 
								then
									True
								else 
									False

main = do
	xString <- getLine
	yString <- getLine

	let x = (read xString :: Int)
	let y = (read yString :: Int)

	print (coprimos x y)									
