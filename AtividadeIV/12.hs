-- Crie uma função que receba três números x, y e z e retorne o máximo divisor comum (DICA: apenas
-- modifique o algoritmo anterior). Leia x, y e z do teclado.

--OK

mdc :: Int -> Int -> Int
mdc a b | a < b = mdc b a
        | b == 0 = a
        | otherwise = mdc b (mod a b)

mdc3 :: Int -> Int -> Int -> Int
mdc3 a b c = mdc a (mdc b c)

main = do
	aString <- getLine
	bString <- getLine
	cString <- getLine

	let a = (read aString :: Int)
	let b = (read bString :: Int)
	let c = (read cString :: Int)

	print (mdc3 a b c)