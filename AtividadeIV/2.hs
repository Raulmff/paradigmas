-- Crie uma função que receba um número x, negativo ou positivo, e retorne seu valor absoluto. Leia x do teclado

--OK

absoluto :: Float -> Float
absoluto x = (abs x)

main = do
	xString <- getLine
	let x = (read xString :: Float)

	print (absoluto x)