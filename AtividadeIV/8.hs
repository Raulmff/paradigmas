-- Crie uma função que resolva uma equação de segundo grau da forma ax 2 + bx + c utilizando a fórmula
-- de Bhaskara. Leia os coeficientes a, b e c do teclado.

--OK

delta :: Float -> Float -> Float -> Float
delta a b c = (b*b) - (4*a*c)

basPos :: Float -> Float -> Float -> Float
basPos a b c = ( (-b) + (sqrt (delta a b c)) ) / (2*a)

basNeg :: Float -> Float -> Float -> Float
basNeg a b c = ( (-b) - (sqrt (delta a b c)) ) / (2*a)

main = do
	aString <- getLine
	bString <- getLine
	cString <- getLine

	let a = (read aString :: Float)
	let b = (read bString :: Float)
	let c = (read cString :: Float)

	print (basPos a b c)
	print (basNeg a b c)