-- Crie uma função que receba dois números x e y e retorne se x é divisı́vel por y. Leia x e y do teclado.

--OK

ehDiv :: Int -> Int -> Bool
ehDiv x y = if ( x `rem` y  == 0)
								then 
									True
								else
									False


main = do
	xString <- getLine
	yString <- getLine
	let x = (read xString :: Int)
	let y = (read yString :: Int)

	print (ehDiv x y)
	