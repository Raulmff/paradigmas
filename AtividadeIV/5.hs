-- Crie uma função que receba três notas de um aluno (a, b, c), calcule a média e retorne se o aluno foi
-- aprovado ou reprovado. Para um aluno ser aprovado, ele deve possuir nota igual ou superior a 6. Leia as
-- notas dos alunos do teclado.

--PROBLEMA NA MEDIA!!!!!

media :: Float -> Float -> Float -> Float
media n1 n2 n3 = (n1 * n2 * n3)/3

aprovado :: Float -> String
aprovado media = if media < 6
							then 
								"Reprovado"
							else
								"Aprovado"


main = do
	n1String <- getLine
	n2String <- getLine
	n3String <- getLine

	let n1 = (read n1String :: Float)
	let n2 = (read n1String :: Float)
	let n3 = (read n1String :: Float)


	print (aprovado (media n1 n2 n3))