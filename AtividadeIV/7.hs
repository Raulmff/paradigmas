-- Crie uma função que compute o n-ésimo número de Fibonacci. Leia n do teclado.

--OK

fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

main = do
	nString <- getLine

	let n = (read nString :: Int)

	print (fib n)