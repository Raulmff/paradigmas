;Crie uma estrutura de dados para pontos 2D, a qual deve possuir os campos x e y.
;Crie uma função distancia (a b), a qual recebe dois pontos como parâmetro de deve retornar a
;distância entre eles.

;OK

(defstruct ponto
	x
	y
)

(setq a
	(make-ponto
		:x 1
		:y 3
	)
)

(setq b
	(make-ponto
		:x 1
		:y 5
	)
)

(defun distancia (a b)
	(sqrt (+ (expt (- (ponto-x a) (ponto-x b)) 2) (expt (- (ponto-y a) (ponto-y b)) 2)))
)

(defun main()
	(write-line (write-to-string (distancia a b)))
)

(main)