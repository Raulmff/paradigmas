; Motifique o arquivo arvore.lisp (dispon´ıvel no Moodle) de forma a adicionar novas opera¸c~oes a nossa
; ´arvore. Assuma que nossa ´arvore n~ao ´e uma ´arvore bin´aria de busca.

(defstruct no
    n
    esq
    dir
)

(setq minhaArvore
    (make-no 
        :n 52
        :esq (make-no :n 32               ;pode omitir o NIL 
                      :esq (make-no :n 12 :esq NIL :dir NIL) 
                      :dir (make-no :n 35 :esq NIL :dir NIL)
             )
        :dir (make-no :n 56 
                      :esq (make-no :n 55 :esq NIL :dir NIL) 
                      :dir (make-no :n 64 :esq NIL :dir NIL)
             ) 
    )
)

(defun soma (arv)
    (if (null arv)
        0
        (+ 
            (no-n arv) 
            (soma (no-esq arv)) 
            (soma (no-dir arv))
        )
    )
)

(defun buscaElemento (arv x)
    (if (null arv)
        NIL
        (or 
            (= (no-n arv) x)
            (buscaElemento (no-esq arv) x) 
            (buscaElemento (no-dir arv) x)
        )
    )
)

(defun minimo (x y)
    (if (< x y)
        x
        y
    )
)

(setq INF 1000)

(defun minimoElemento (arv)
    (if (null arv)
        INF
        (minimo 
            (no-n arv) 
            (minimo 
                (minimoElemento (no-esq arv)) 
                (minimoElemento (no-dir arv))
            )
        )
    )
)

(defun incrementa (arv x)
    (if (not (null arv))
        (progn
            (setf (no-n arv) (+ (no-n arv) x))
            (incrementa (no-esq arv) x)
            (incrementa (no-dir arv) x)
        )
    )
)

; OK
; A: Crie uma fun¸c~ao com a seguinte assinatura: ocorrenciasElemento (arv x), a qual recebe um
; n´umero e deve retornar a quantidade de ocorr^encias dele na ´arvore.
(defun ocorrenciasElemento (arv x)
	(if (null arv)
		0
		(if (= (no-n arv) x)
			(+ 1 (ocorrenciasElemento (no-esq arv) x) (ocorrenciasElemento (no-dir arv) x))
			(+ 0 (ocorrenciasElemento (no-esq arv) x) (ocorrenciasElemento (no-dir arv) x))
		)
	)
)

;OK
; B: Crie uma fun¸c~ao com a seguinte assinatura: maioresQueElemento (arv x), a qual recebe um n´umero
; e deve retornar a quantidade de n´umeros maiores que ele na ´arvore.
(defun maioresQueElemento (arv x)
	(if (null arv)
        0
        (if (> (no-n arv) x)
            (+ 1 (maioresQueElemento (no-esq arv) x) (maioresQueElemento (no-dir arv) x))
            (+ (maioresQueElemento (no-esq arv) x) (maioresQueElemento (no-dir arv) x))
        )
    )
)

;OK
; C: Crie uma fun¸c~ao com a seguinte assinatura: mediaElementos (arv), a qual deve retornar a m´edia
; dos n´umeros na ´arvore.
(defun quantidade (arv)
    (if (null arv)
        0
        (+ 1 (quantidade (no-esq arv)) (quantidade (no-dir arv)))
    )
)
(defun mediaElementos (arv)
    (/ (soma arv) (quantidade arv))
)

;OK
; D: Crie uma fun¸c~ao com a seguinte assinatura: quantidade (arv), a qual deve retornar a quantidade
; de elementos na ´arvore.
(defun quantidade (arv)
    (if (null arv)
        0
        (+ 1 (quantidade (no-esq arv)) (quantidade (no-dir arv)))
    )
)

;OK
; E: Crie uma fun¸c~ao com a seguinte assinatura: elementos (arv), a qual deve retornar uma lista com
; todos os elementos na ´arvore.
(defun elementos (arv) 
    (if (null arv)
        '()
        (cons (no-n arv) (concatenate 'list (elementos (no-esq arv)) (elementos (no-dir arv))))
    )
)

;OK
; F: Crie uma fun¸c~ao com a seguinte assinatura: substituir (arv x y), a qual recebe dois n´umeros como
; par^ametro e deve buscar e alterar todas as ocorr^encias de x pelo valor y.
(defun substituir (arv x y)
    (if (not(null arv))
        (if (= (no-n arv) x)
            (progn
                (setf (no-n arv) y)
                (substituir (no-esq arv) x y)
                (substituir (no-dir arv) x y)
            )
            (progn
                (substituir (no-esq arv) x y)
                (substituir (no-dir arv) x y)
            )
        )
    )
    arv
)

;OK
; G: Crie uma fun¸c~ao com a seguinte assinatura: posordem (arv), a qual deve retornar a lista de n´umeros
; visitados em percorrimento p´os-ordem.
(defun posordem (arv)
    (if (not (null arv))
        (nconc (nconc (posordem (no-esq arv)) (posordem (no-dir arv))) (list (no-n arv))))
)

;OK
; H: Crie uma fun¸c~ao com a seguinte assinatura: preordem (arv), a qual deve retornar a lista de n´umeros
; visitados em percorrimento pr´e-ordem.
(defun preordem (arv)
    (if (not (null arv))
        (nconc (nconc (list (no-n arv)) (posordem (no-esq arv))) (posordem (no-dir arv))))
)

;OK
; I: Crie uma fun¸c~ao com a seguinte assinatura: emordem (arv), a qual deve retornar a lista de n´umeros
; visitados em percorrimento em-ordem.
(defun emordem (arv)
    (if (not (null arv))
        (nconc (nconc (posordem (no-esq arv)) (list (no-n arv))) (posordem (no-dir arv))))
)

;OK
; J: Crie uma fun¸c~ao com a seguinte assinatura: subtraiParesImpares (arv), a qual deve retornar a
; soma de todos os n´umeros pares - (menos) a soma de todos os n´umeros ´ımpares.
(defun somaPares (arv)
    (if (null arv)
        0
        (if (= (mod (no-n arv) 2) 0)
            (+ 
                (no-n arv) 
                (somaPares (no-esq arv)) 
                (somaPares (no-dir arv))
            )
            (+ 
                (somaPares (no-esq arv)) 
                (somaPares (no-dir arv))
            )
        )
    )
)
(defun somaImpares (arv)
    (if (null arv)
        0
        (if (/= (mod (no-n arv) 2) 0)
            (+ (no-n arv) (somaImpares (no-esq arv)) (somaImpares (no-dir arv)))
            (+ (somaImpares (no-esq arv)) (somaImpares (no-dir arv)))
        )
    )
)
(defun subtraiParesImpares (arv)
    (- (somaPares arv) (somaImpares arv))
)

;OK
; K: Crie uma fun¸c~ao com a seguinte assinatura: novoNo (x), a qual recebe um n´umero como par^ametro
; e deve retornar uma nova ´arvore com apenas o n´o contendo o valor x.
(defun novoNo (x)
    (make-no 
        :n x
        :esq NIL
        :dir NIL 
    )
)

;OK
; L: Crie uma fun¸c~ao com a seguinte assinatura: inserir (arv x), a qual recebe um n´umero como
; par^ametro deve inser´ı-lo na ´arvore respeitando as seguintes condi¸c~oes:
; • Tente adicion´a-lo primeiramente em um n´o interno com ou a ´arvore direita ou a ´arvore esquerda
; nula (mas n~ao ambas nulas).
; • Se n~ao for poss´ıvel, transforme uma folha em um n´o interno adicionando x ou no lado esquerdo
; ou no lado direito do n´o.
; DICA: crie algumas fun¸c~oes auxiliares para facilitar.
(defun inserir (arv x)
    (if (not(null arv))
        (if (and (null (no-esq arv)) (not (null (no-dir arv))))
            (setf (no-esq arv) (novoNo x))
            (if (and (not(null (no-esq arv))) (null (no-dir arv)))
                (setf (no-dir arv) (novoNo x))
                (if (and (null (no-esq arv)) (null (no-dir arv)))
                    (setf (no-esq arv) (novoNo x))
                    (progn
                        (or (inserir (no-esq arv) x) (inserir (no-dir arv) x))
                    )
                )
            )
        )
    )
)

(defun main()
    ;(write-line (write-to-string (soma minhaArvore)))
    ;(write-line (write-to-string (buscaElemento minhaArvore 35)))
    ;(write-line (write-to-string (buscaElemento minhaArvore 36)))
    ;(write-line (write-to-string (minimoElemento minhaArvore)))
    ;(write-line (write-to-string (incrementa minhaArvore 2)))
    ;(write-line (write-to-string minhaArvore))
    ;(write-line (write-to-string (ocorrenciasElemento minhaArvore 35)))
    ;(write-line (write-to-string (maioresQueElemento minhaArvore 12)))
    ;(write-line (write-to-string (mediaElementos minhaArvore)))
    (write-line (write-to-string (elementos minhaArvore)))
    (write-line (write-to-string (inserir minhaArvore 12356)))
    (write-line (write-to-string (elementos minhaArvore)))
    ;(write-line (write-to-string (substituir minhaArvore 12 120)))
    ;(write-line (write-to-string (somaPares minhaArvore)))
    ;(write-line (write-to-string (somaImpares minhaArvore)))
    ;(write-line (write-to-string (subtraiParesImpares minhaArvore)))
    ;(write-line (write-to-string (novoNo 12)))
    ;(write-line (write-to-string (posordem minhaArvore)))
    ;(write-line (write-to-string (preordem minhaArvore)))
    ;(write-line (write-to-string (emordem minhaArvore)))
)

(main)