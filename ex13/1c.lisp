;Crie uma estrutura de dados para pontos 2D, a qual deve possuir os campos x e y.
;Crie uma função formaTriangulo (a b c), a qual recebe três pontos como parâmetro de deve retor-
;nar se eles podem ser usados para formar um triângulo.

;OK

(defstruct ponto
	x
	y
)

(setq a
	(make-ponto
		:x 0
		:y 5
	)
)

(setq b
	(make-ponto
		:x 1
		:y 3
	)
)

(setq c
	(make-ponto
		:x 2
		:y 2
	)
)

(defun determinante (a b c)
	(- (+ (* (ponto-x a) (ponto-y b)) (* (ponto-y a) (ponto-x c)) (* (ponto-x b) (ponto-y c))) 
		(+ (* (ponto-y a) (ponto-x b)) (* (ponto-x a) (ponto-y c)) (* (ponto-y b) (ponto-x c))) )

)

(defun colineares (a b c)
	(if (= (determinante a b c) 0)
		T 
		NIL 
	)

)

(defun formaTriangulo (a b c)
	(if (not (colineares a b c)) 
		T
		NIL
	)

)

(defun main()
	(write-line (write-to-string (formaTriangulo a b c)))
)

(main)