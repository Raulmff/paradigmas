-- Crie um m´odulo para as funcionalidades de ´arvores vistas em aula e resolvidas nos exerc´ıcios de listas
-- anteriores. Fa¸ca tamb´em um m´odulo Main para exemplificar o uso do m´odulo da ´arvore.

--OK

import Arvore

main = do 
	-- putStrLn (show (somaElementos minhaArvore))
    -- putStrLn (show (buscaElemento minhaArvore 30))
    -- putStrLn (show (buscaElemento minhaArvore 55))
    -- putStrLn (show (minimoElemento minhaArvore))
    print (ocorrenciasElemento minhaArvore 52)
    print (maioresQueElemento minhaArvore 52)
    print (quantidade minhaArvore)
    print (mediaElementos minhaArvore)
    print (elementos minhaArvore)