module Pilha (Pilha (Stack), emptyStack, push, pop, top, comprimento, igual, soma, busca, minimo) where

data Pilha t = Stack [t]
    deriving (Eq,Show)
    
push :: Pilha t -> t -> Pilha t
push (Stack s) x = Stack (x:s)

pop :: Pilha t -> Pilha t
pop (Stack []) = error "Empty"
pop (Stack (x:s)) = Stack s

top :: Pilha t -> t
top (Stack []) = error "Empty"
top (Stack (x:s)) = x

emptyStack :: Pilha t
emptyStack = Stack []

-- comprimento :: Pilha t -> Int
-- comprimento (Stack []) = 0
-- comprimento (Stack (a:b)) = 1 + ( comprimento (Stack b) )

-- igual :: Pilha Float -> Pilha Float -> Bool
-- igual (Stack []) (Stack []) = True
-- igual (Stack []) _ = False
-- igual _ (Stack []) = False
-- igual (Stack (a:b)) (Stack (c:d)) =
-- 	if (a == c)
-- 		then (igual (Stack b) (Stack d))
-- 		else False

-- soma :: Pilha Float -> Float
-- soma (Stack []) = 0
-- soma (Stack (a:b)) = a + (soma (Stack b))

-- busca :: Pilha Float -> Float -> Bool
-- busca (Stack []) _ = False
-- busca (Stack (a:b)) x =
-- 	if ( a == x )
-- 		then True
-- 		else (busca (Stack b) x)

-- minimo :: Pilha Float -> Float -> Float
-- minimo (Stack []) x = x
-- minimo (Stack (a:b)) x = 
-- 	if (a < x)
-- 		then (minimo (Stack b) a)
-- 		else (minimo (Stack b) x)
