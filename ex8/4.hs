
-- Explore os arquivos principal.hs e pilha.hs dispon´ıveis no Moodle. Entenda seu funcionamento, tente
-- compilar e executar em seu computador. Teste tamb´em outros exemplos. Depois fa¸ca:

import Fila 

main = do 

    print "Enfileirando 4 em (Stack [1,2,3])"
    print (enqueue (Stack [1,2,3]) 4)

    print "Desenfileirando um elemento (Stack [1,2,3,4])"
    print (dequeue (Stack [1,2,3,4]))

    print "Primeiro da fila em (Stack [2,3,4])"
    print (first (Stack [2,3,4]))



    

