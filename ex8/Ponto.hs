-- Crie um módulo chamado Ponto, o qual deve definir o tipo Ponto, que é um Ponto2D formado por uma
-- tupla (Float, Float), ou seja, data Ponto = Ponto2D (Float, Float). Crie um módulo Main para testar
-- o módulo Ponto. Neste módulo crie as seguintes funções:

module Ponto (Ponto (Ponto2D), distancia, colineares, formaTriangulo) where

data Ponto = Ponto2D Float Float
	deriving Show

-- distancia :: Ponto -> Ponto -> Float, a qual deve retornar a distância entre dois pontos.
distancia :: Ponto -> Ponto -> Float
distancia (Ponto2D x1 y1) (Ponto2D x2 y2) = ( sqrt ((x2 - x1)**2 + (y2-y1)**2) )

-- colineares :: Ponto -> Ponto -> Ponto -> Bool, a qual deve verificar se os três pontos são
-- colineares. Os três pontos são colineares se o determinante referente a esses pontos é zero.
colineares :: Ponto -> Ponto -> Ponto -> Bool
colineares p1 p2 p3 =
	if ( (determinante p1 p2 p3) == 0 )
		then True
		else False 

determinante :: Ponto -> Ponto -> Ponto -> Float
determinante (Ponto2D x1 y1) (Ponto2D x2 y2) (Ponto2D x3 y3) =
	( (x1 * y2) + (y1*x3) + (x2*y3)) - ( (y1*x2) + (x1*y3) + (y2*x3) )

-- formaTriangulo :: Ponto -> Ponto -> Ponto -> Bool, a qual deve verificar se os três pontos
-- podem formar um triângulo.
formaTriangulo :: Ponto -> Ponto -> Ponto -> Bool
formaTriangulo p1 p2 p3 =
	if ((colineares p1 p2 p3) == True)
		then False
		else if ( (pIgual p1 p2) || (pIgual p2 p3) || (pIgual p1 p3) )
			then False
			else True

pIgual :: Ponto -> Ponto -> Bool
pIgual (Ponto2D x1 y1) (Ponto2D x2 y2) = 
	if ( (x1 == x2) && (y1 == y2) )
		then True
		else False
