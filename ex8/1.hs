-- Modifique o módulo de Formas visto em sala de aula e disponı́vel nos slides para que suporte o cálculo
-- de área de um trapézio. Portanto, crie uma nova forma para o Trapézio, exporte ela e faça um teste no
-- módulo Main.

--ok

import Formas

main = do
	print (area (Retangulo 5 2))
	print (area (Circulo 5))
	print (area (Trapezio 5 10 6))
