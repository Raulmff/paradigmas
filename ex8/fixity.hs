
-- Pesquise sobre defini¸c~ao de preced^encias no Haskell (fixity). Fa¸ca um breve resumo e um pequeno
-- exemplo ilustrativo. Utilize m´odulos em seu exemplo. SUGESTAO ~ : utilize os operadores *** e +++ e
-- classes j´a criados em aulas anteriores. Entregue um .zip com seus arquivos .hs de exemplo.

-- O fixity define uma precedencia para as operacoes. Essa precedencia eh definida com um
-- valor de 0 a 9, sendo 9 a maior e 0 a menor.

--OK

infixr  6 +++
infixr  7 ***

(+++) :: Int -> Int -> Int
a +++ b = a + 2*b

(***) :: Int -> Int -> Int
a *** b = a + 4*b

main = do
	print "Calculando (1 +++ 2 *** 3), cujo resultado deve ser (1 + 2*(2+(4*3)))"
	print (1 +++ 2 *** 3)
	print "Calculando (1 *** 2 +++ 3), cujo resultado deve ser ((1+(2*4)) + (2*3))"
	print (1 *** 2 +++ 3)
