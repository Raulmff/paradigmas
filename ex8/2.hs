-- Crie um módulo chamado Ponto, o qual deve definir o tipo Ponto, que é um Ponto2D formado por uma
-- tupla (Float, Float), ou seja, data Ponto = Ponto2D (Float, Float). Crie um módulo Main para testar
-- o módulo Ponto. 

--ok

module Main (main) where

import Ponto

main = do
	print ("Distancia de (1,1) e (1,4)")
	print (distancia (Ponto2D 1 1) (Ponto2D 1 4))

	print ("Colineares (0,5), (1,3) e (2,1) ?")
	print (colineares (Ponto2D 0 5) (Ponto2D 1 3) (Ponto2D 2 1) )

	print ("Colineares (2, 2), (-3, -1) e (-3, 1) ?")
	print (colineares (Ponto2D 2 2) (Ponto2D (-3) (-1)) (Ponto2D (-3) (1)) )

	print ("Forma triangulo (0,5), (1,3) e (2,1)?")
	print (formaTriangulo (Ponto2D 0 5) (Ponto2D 1 3) (Ponto2D 2 1) )

	print ("Forma triangulo (2, 2), (-3, -1) e (-3, 1)?")
	print (formaTriangulo (Ponto2D 2 2) (Ponto2D (-3) (-1)) (Ponto2D (-3) (1)) )
