
-- Crie um m´odulo para a estrutura de dados Fila.
module Fila (Fila (Stack), enqueue, dequeue, first, emptyQueue) where

-- Crie a estrutura para a Fila (Queue).
data Fila t = Stack [t]
    deriving (Eq,Show)

-- Crie a opera¸c~ao para enfileirar algum elemento (enqueue).
-- A operacao de enfileirar adiciona um elemento no final da fila (do lado "direito")
enqueue :: Fila t -> t -> Fila t
enqueue (Stack (s)) x = Stack (s ++ [x])

-- Crie a opera¸c~ao para desenfileirar algum elemento (dequeue).
-- A operacao de desenfileirar eh exatamente igual ao da Pilha
-- Retira o elemento do comeco da fila (mais a esquerda)
dequeue :: Fila t -> Fila t
dequeue (Stack []) = error "Empty"
dequeue (Stack (x:s)) = Stack s

-- Crie a opera¸c~ao para retornar o primeiro elemento da fila (first).
-- O primeiro da fila eh o mesmo do topo da pilha
first :: Fila t -> t
first (Stack []) = error "Empty"
first (Stack (x:s)) = x

-- Crie a opera¸c~ao para criar uma fila vazia (emptyQueue).
-- Mesmo metodo da pilha
emptyQueue :: Fila t
emptyQueue = Stack []

