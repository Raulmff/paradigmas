; Crie  uma  fun ̧c ̃ao segundoMenor(v)
; que  receba  uma  lista  como  parˆametro  e  retorne  o  segundo  menor
; elemento nele.

;OK

(defun menor (lista nMenor)
	(if (not(null lista))
		(if (< (first lista) nMenor)
			(menor (rest lista) (first lista))
			(menor (rest lista) nMenor)
		)
		nMenor
	)
)

(defun retiraItemLista (item lista)
	(if (not(null lista))
		(if (= (first lista) item)
			(retiraItemLista item (rest lista))
			(cons (first lista) (retiraItemLista item (rest lista)))
		)
		'()
	)
)

(defun segundoMenor (lista)
	(progn
		(setf menorElemento (menor lista 123456789))
		(menor (retiraItemLista menorElemento lista) 123456789)
	)
)

(defun main ()
	(setf lista '(1 4 7 3 8 9 3 5 6))
	(setf lista2 '(4 5 7 3 9 12))

	(write-line (write-to-string (segundoMenor lista)))
	(write-line (write-to-string (segundoMenor lista2)))
)

(main)