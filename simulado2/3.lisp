; Crie uma fun ̧c ̃ao interssecao(a b) , a qual recebe duas listas de n ́umeros inteiros (A e B) como parˆametro
; e deve retornar a intersse ̧c ̃ao entre elas, ou seja, uma terceira lista C com somente os elementos que est ̃ao
; em A e B. A lista C deve conter uma  ́unica vez cada n ́umero.

;OK

(defun interseccao (a b resposta)
	(if (not (null a))
		(if (existe (first a) b)
			(if (member (first a) resposta)
				(interseccao (rest a) b resposta)
				(interseccao (rest a) b (cons (first a) resposta))
			)
			(interseccao (rest a) b resposta)
		)
		resposta
	)
)

(defun existe (n lista)
	(if (null lista)
		NIL
		(if (= n (first lista))
			T
			(existe n (rest lista))
		)
	)
)

(defun main ()
	(setf a '(1 4 7 3 8 9 3 5 6))
	(setf b '(5 6 7 2 4 1 8 6))

	(write-line (write-to-string (interseccao a b '())))
)

(main)