;Motifique o arquivo arvore.hs
;(disponıvel no Moodle, na atividade sobre estruturas em LISP) de forma
;a adicionar novas opera ̧c ̃oes a nossa  ́arvore:

;A:
;Crie uma fun ̧c ̃ao com a seguinte assinatura:
;maioresQueElemento(arv, x)
;, a qual recebe um n ́umero
;e deve retornar a quantidade de n ́umeros maiores que ele na  ́arvore.

;B:
;Crie  uma  fun ̧c ̃ao  com  a  seguinte  assinatura:
;pares(arv)
;,  a  qual  deve  retornar  uma  lista  contendo
;todos os n ́umeros pares da  ́arvore.

;C:
;Crie uma fun ̧c ̃ao com a seguinte assinatura:
;folhas(arv)
;, a qual deve retornar a quantidade de folhas
;na  ́arvore.  Um n ́o  ́e uma folha se ele n ̃ao possui filhos.

;D:
;Crie  uma  fun ̧c ̃ao  com  a  seguinte  assinatura:
;maiorFinonacci(arv) ,  a  qual  deve  retornar  o  maior
;n ́umero  da  sequˆencia  de  Fibonacci  encontrado  na   ́arvore.   Caso  n ̃ao  haja  nenhum,  retorne  -1.   O
;n- ́esimo n ́umero de Fibonacci pode ser descoberto utilizando a seguinte f ́ormula: