; Crie  uma  fun ̧c ̃ao  chamada rotacionar(a, n),  que  receba  uma  matriz  como  parˆametro  e  rotacione  a
; matriz para a direita n vezes,  sendo n
; tamb ́em recebido como parˆametro.  O resultado da fun ̧c ̃ao deve
; ser a matriz rotacionada n vezes para a direita.  Por exemplo, para a entrada abaixo e 2 rota ̧c ̃oes deve-se
; obter o resultado abaixo

;OK

(defun rotacionar (matriz tamanho n)
	(dotimes (i 4) 
		(progn
			(setf lista '())
	        (dotimes (j 3)
	            (setf lista (concatenate 'list lista (list (aref matriz i j))))
	        )
        	;(setf matriz (modificarLinha matriz i lista n tamanho 0))
        	(write-line (write-to-string lista))
        	(modificarLinha matriz i lista n tamanho 0)
		)
    )
)

(defun modificarLinha (matriz linha listaElem n tamanho posicao)  
	(if (not(null listaElem))
		(progn
			(setf (aref matriz linha (mod (+ posicao n) tamanho)) (first listaElem))
			(modificarLinha matriz linha (rest listaElem) n tamanho (+ 1 posicao))
		)
	)
	matriz
)

(setf matriz (make-array '(4 3) ;criando uma matriz 3x3
   :initial-contents '((1 2 3) (4 5 6) (7 8 9) (10 11 12))) ;inicializando a matriz com alguns valores
)

(defun main ()
	(write-line (write-to-string (rotacionar matriz 3 1)))
	(write-line (write-to-string matriz))
)

(main)