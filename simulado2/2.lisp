; Crie  uma  fun ̧c ̃ao pell(n) que  retorne  o  n- ́esimo  n ́umero  de  Pell.   O  n- ́esimo  n ́umero  de  Pell  pode  ser
; descoberto utilizando a seguinte f ́ormula:

;OK

(defun pell(n)
	(if (= n 0)
		0
		(if (= n 1)
			1
			(+ (* 2 (pell (- n 1))) (pell (- n 2)))
		) 
	)
)

(defun main ()
	(write-line (write-to-string (pell 2)))
)

(main)