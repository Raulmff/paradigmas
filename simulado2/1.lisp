; Crie  uma  fun ̧c ̃ao sequencia(n) que  retorne  o  n- ́esimo  n ́umero  da  sequˆencia  5,  11,  19,  29,  41,  55,  ...
; (considere que 5  ́e o elemento inicial da sequˆencia)

(defun sequencia (n)
	(if (= n 1)
		5
		(+ (sequencia (- n 1)) (+ 2 (* n 2)))
	)
)

(defun main ()
	(write-line (write-to-string (sequencia 2)))
	(write-line (write-to-string (sequencia 3)))
	(write-line (write-to-string (sequencia 4)))
	(write-line (write-to-string (sequencia 5)))
	(write-line (write-to-string (sequencia 6)))
)

(main)