;Dizemos que uma matriz quadrada inteira
;A nxn  ́e um quadrado m ́agico se a soma dos elementos de uma
;determinada  linha,  coluna  ou  diagonal   ́e  sempre  igual.   Fa ̧ca  que  receba  como  
;parˆametro  uma  matriz
;com alguns n ́umeros do quadrado m ́agico j ́a preenchidos e retorne uma matriz com o quadrado m ́agico
;completo.  Considere que n ́umeros v ̃ao de 1 at ́e 1000 (inclusive) e podem se repetir.  
;As posi ̧c ̃oes da matriz
;com 0 indicam que aquela posi ̧c ̃ao n ̃ao est ́a preenchida.  Abaixo, s ̃ao ilustrados dois exemplos 
;de matrizes
;dadas  como  entrada  e  o  resultado  esperado  da  sua  fun ̧c ̃ao.   Note  que  podem  
;existir  v ́arios  resultados
;v ́alidos, mas tamb ́em pode ocorrer de n ̃ao existir uma solu ̧c ̃ao para a matriz dada.  
;Neste caso, retorne
;uma matriz toda zerada.

