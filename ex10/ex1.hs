-- Altere o exemplo visto em sala sobre a operação Div para suportar também as operações 
-- Mul Expr Expr para multiplicação)
-- Add Expr Expr (para soma) 
-- Sub Expr Expr (para subtração)
-- Assim, sua solução deve suportar expressões como (Mul (Div (Add (Val 28) (Val 2)) (Sub (Val 6) (Val 1))) (Val 3)). Faça
-- as modificações necessárias nas funções de avaliação criadas e crie ao menos três exemplos de expressões
-- para testar sua solução. As modificações devem ser feitas nas três formas de criar esta função vistas em
-- sala de aula (sem usar monads, usando >>= e usando a notação do. Não preocupe-se com precedência
-- de operadores. Qual forma foi mais fácil de dar manutenção? E mais difı́cil?

data Expr = Val Int | Div Expr Expr | Mul Expr Expr | Sub Expr Expr | Add Expr Expr

mydiv :: Int -> Int -> Maybe Int
mydiv n m 
	| m == 0 = Nothing
	| otherwise = Just (n `div` m)

mymult :: Int -> Int -> Maybe Int
mymult n m = Just (n * m)

myadd :: Int -> Int -> Maybe Int
myadd n m = Just (n + m)

mysub :: Int -> Int -> Maybe Int
mysub n m = Just (n - m)

eval :: Expr -> Maybe Int
eval (Val n) = Just n
eval (Div x y) = case (eval x) of
					Nothing -> Nothing
					Just n -> case (eval y) of
						Nothing -> Nothing
						Just m -> (mydiv n m)
eval (Mul x y) = case (eval x) of
					Nothing -> Nothing
					Just n -> case (eval y) of
						Nothing -> Nothing
						Just m -> (mymult n m)	
eval (Add x y) = case (eval x) of
					Nothing -> Nothing
					Just n -> case (eval y) of
						Nothing -> Nothing
						Just m -> (myadd n m)	
eval (Sub x y) = case (eval x) of
					Nothing -> Nothing
					Just n -> case (eval y) of
						Nothing -> Nothing
						Just m -> (mysub n m)																							

main = do
	print "divisao de 5 por 0"
	print (eval (Div (Val 5) (Val 0)))
	print "divisao de 10 por 2"
	print (eval (Div (Val 10) (Val 2)))
	print "multiplicacao de 2 por 5"
	print (eval (Mul (Val 2) (Val 5)))	
	print "adicao de 1 por 2"	
	print (eval (Add (Val 1) (Val 2)))	
	print "subtracao de 2 por 1"	
	print (eval (Sub (Val 2) (Val 1)))	
	print "funcao da questao"
	print (eval (Mul (Div (Add (Val 28) (Val 2)) (Sub (Val 6) (Val 1))) (Val 3)))