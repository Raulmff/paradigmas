;Crie uma fun¸c˜ao que receba um n´umero n e retorne se o mesmo ´e primo. Leia n do teclado.

;OK

(defun ehPrimo (n d) 
  	(if (eq d 1) 
      	T
      	(if (eq (mod n d) 0) 
          	NIL
          	(ehPrimo n (- d 1))
        )
   	)
)

(defun main()
	(setq n (read))
	(write-line (write-to-string (ehPrimo n (- n 1))))
)

(main)