;Crie uma fun¸c˜ao que resolva uma equa¸c˜ao de segundo grau da forma ax2 + bx + c utilizando a f´ormula
;de Bhaskara. Leia os coeficientes a, b e c do teclado.

;OK

(defun delta (a b c)
	(sqrt (- (expt b 2) (* 4 (* a c))))
)

(defun bhaskara1 (a b c)
	(/ (+ (- b) (delta a b c)) (* 2 a))
)

(defun bhaskara2 (a b c)
	(/ (- (- b) (delta a b c)) (* 2 a))
)

(defun main()
	(setq a (read))
	(setq b (read))
	(setq c (read))
	(write-line (write-to-string (bhaskara1 a b c)) )
	(write-line (write-to-string (bhaskara2 a b c)) )
)

(main)