;Crie uma fun¸c˜ao que receba dois n´umeros x e y e retorne se x ´e divis´ıvel por y. Leia x e y do teclado.

;OK

(defun divisivel (a b)
	(if (eq (mod a b) 0)
		T 
		NIL
	)
)

(defun main()
	(setq a (read))
	(setq b (read))
	(write-line (write-to-string (divisivel a b)))
)

(main)