;Crie uma fun¸c˜ao que receba 3 valores num´ericos (a, b, c) e retorne o maior deles. N˜ao utilize nenhuma
;forma de ordena¸c˜ao. Leia os valores a, b, c do teclado.

;OK

(defun maior (a b c)
	(if (and (> a b) (> a c))
		a
		(if (and (> b c) (> b a))
			b
			c
		)
	)
)

(defun main()
	(setq a (read))
	(setq b (read))
	(setq c (read))
	(write-line (write-to-string (maior a b c)))
)

(main)