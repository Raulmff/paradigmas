; Crie uma fun¸c˜ao que receba trˆes inteiros x, y e z e retorne se havendo varetas com esses valores em
; comprimento pode-se construir um triˆangulo. Exemplo, com varetas de comprimento 4, 8 e 9 posso
; construir um triˆangulo, por´em com varetas de comprimento 10, 5 e 4 n˜ao posso construir um triˆangulo.
; Leia x, y e z do teclado.

;OK

(defun existeTriangulo (n1 n2 n3)
	(if ( and (> n1 (abs(- n2 n3))) (< n1 (+ n2 n3)) )
		T
		(if ( and (> n2 (abs(- n1 n3))) (< n2 (+ n1 n3)) )
			T
			(if ( and (> n3 (abs(- n1 n2))) (< n3 (+ n1 n2)) )
				T
				NIL
			)
		)
	)
)

(defun main()
	(setq n1 (read))
	(setq n2 (read))
	(setq n3 (read))
	(write-line (write-to-string (existeTriangulo n1 n2 n3)) )

)

(main)