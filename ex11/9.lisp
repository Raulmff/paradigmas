;Crie uma fun¸c˜ao que dados dois pontos no espa¸co 3D, (x1, y1, z1) e (x2, y2, z2), compute a distˆancia
;entre eles. Leia as posi¸c˜oes dos pontos do teclado.

;OK

(defun distancia (a b c d e f)
	(sqrt (+ (expt (- d a) 2) (expt (- e b) 2) (expt (- f c) 2)))
)

(defun main()
	(setq a (read))
	(setq b (read))
	(setq c (read))
	(setq d (read))
	(setq e (read))
	(setq f (read))
	(write-line (write-to-string (distancia a b c d e f)) )
)

(main)