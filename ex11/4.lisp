; Crie uma função que receba dois valores booleanos (x, y) retorne o resultado do “ou exclusivo” (XOR)
; sobre eles. A função apenas deve usar os operadores and, or e not. Leia os valores x e y do teclado.

;OK

(defun meuXor (x y)
	(if (eq x T)
		(not y)
		y
	)
)

(defun main()
	(setq x (read))
	(setq y (read))
	(write-line (write-to-string (meuXor x y)))

)

(main)