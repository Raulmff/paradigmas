;Crie uma fun¸c˜ao que receba trˆes parˆametros Operador, x e y, e retorne o resultado da opera¸c˜ao matem´atica
;x Operador y. Os operadores poss´ıveis de informar s˜ao +, -, *, /. Leia o Operador, x e y do teclado.

;OK

(defun operacao (op a b)
	(if (eq #\+ op)
		(+ a b)
		(if (eq #\- op)
			(- a b)
			(if (eq #\* op)
				(* a b)
				(if (eq #\/ op)
					(/ a b)
					0
				)
			)
		)
	)
)

(defun main()
	(setq op (read-char))
	(setq a (read))
	(setq b (read))
	(write-line (write-to-string (operacao op a b)))
)

(main)