; Crie uma função que receba três notas de um aluno (a, b, c), calcule a média e retorne se o aluno foi
; aprovado ou reprovado. Para um aluno ser aprovado, ele deve possuir nota igual ou superior a 6. Leia as
; notas dos alunos do teclado.

; OK

(defun aprovado (nMedia)
	(if (>= nMedia 6)
		T 
		NIL
	)
)

(defun media (n1 n2 n3)
	(/ (+ (+ n1 n2) n3) 3)
)

(defun main()
	(setq n1 (read))
	(setq n2 (read))
	(setq n3 (read))
	(write-line (write-to-string (media n1 n2 n3)) )
	(write-line (write-to-string (aprovado (media n1 n2 n3)) ))

)

(main)