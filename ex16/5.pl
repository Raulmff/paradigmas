%OK

%Crie uma regra media(L,X), a qual recebe uma lista L de inteiros e retorna a média de todos os elementos
%da lista. Note que X é o resultado da média de todos os elementos de L. Retorne 0 caso a lista for vazia.
%Não utilize nenhuma função pronta to Prolog para esta tarefa.

soma([],0).

soma([H|T],X) :- 
	soma(T,NX),
	X is NX + H.

comprimento([],0).

comprimento([H|T],X) :- 
	comprimento(T,NX),
	X is NX + 1.

media(L,X) :-
	soma(L,Soma),
	comprimento(L,Tamanho),
	X is Soma/Tamanho.

%?- media([1,2,3],X).
%X = 2.

%?- media([4,4,4],X).
%X = 4.

%?- media([3,7,15],X).
%X = 8.333333333333334.