%OK

%Crie uma regra posicao(X,L,P) que receba um elemento X, uma lista L e retorne a posição P do elemento
%X na lista L. Não utilize nenhuma função pronta to Prolog para esta tarefa.

posicao(X,L,P) :- 	
	membro(X,L,CP), 
	P is CP.

membro(X,[X|_],0).

membro(X,[_|T],P) :- 
	membro(X,T,CP), 
	P is CP + 1.

%?- posicao(1,[1,2,3],X).
%X = 0 .

%?- posicao(2,[1,2,3],X).
%X = 1 .

%?- posicao(3,[1,2,3],X).
%X = 2 .

%?- posicao(4,[1,2,3],X).
%false.


