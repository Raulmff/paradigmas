%OK

%Crie uma regra apagar(N,L1,L2), a qual recebe, um número de elementos N, uma lista L1, e retorna uma
%lista L2. Esta função deve remover da lista os N primeiros elementos fornecidos como parâmetro. Não
%utilize nenhuma função pronta to Prolog para esta tarefa.

apagar(0,L1,L1) :- !.

apagar(N,[H|T],L2) :-
	NN is N-1,
	apagar(NN,T,L2).

%?- apagar(1,[1,2,3],X).
%X = [2, 3].

%?- apagar(2,[1,2,3],X).
%X = [3].

%?- apagar(3,[1,2,3],X).
%X = [].