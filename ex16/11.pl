%OK

%Crie uma regra primeiros(N,L1,L2), a qual recebe um número de elementos N, uma lista L1, e retorna
%uma lista L2. Esta função deve retornar uma lista com os N primeiros elementos informados no primeiro
%parâmetro. Não utilize nenhuma função pronta to Prolog para esta tarefa.

primeiros(0,_,L2) :-
	write('L2 = '),
	write(L2).

primeiros(N,[H|T],L2) :-
	NN is N-1,
	append(L2,[H],Resultado),
	primeiros(NN,T,Resultado).

%?- primeiros(2,[1,2,3],[]).
%L2 = [1,2]
%true .

%?- primeiros(1,[1,2,3],[]).
%L2 = [1]
%true .

%?- primeiros(3,[1,2,3],[]).
%L2 = [1,2,3]
%true .