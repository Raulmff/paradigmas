%OK

%Crie uma regra diferencaMaiorMenor(L,X), a qual recebe uma lista L de inteiros e retorna a diferença
%entre o maior e o menor elemento da lista. Note que X é o resultado. Retorne 0 caso a lista for vazia.
%Não utilize nenhuma função pronta to Prolog para esta tarefa.

menor([X], X) :- !.
menor([X,Y|T],N) :-
    (X > Y ->
        menor([Y|T],N);
        menor([X|T],N)
    ).

maior([X], X) :- !.
maior([X,Y|T],N) :-
    (X < Y ->
        maior([Y|T],N);
        maior([X|T],N)
    ).

diferencaMaiorMenor([],0).

diferencaMaiorMenor(L,X) :-
	menor(L,Menor),
	maior(L,Maior),
	X is (Maior - Menor).

%?- diferencaMaiorMenor([1,2,3,4],X).
%X = 3.

%?- diferencaMaiorMenor([3,7,3,4],X).
%X = 4.