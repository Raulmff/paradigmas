%OK

%Crie uma regra dividir(L1,L2,L3), a qual recebe uma lista como entrada L1 e deve dividı́-la em duas
%listas L2 e L3 com a mesma quantidade de elementos (exceto quando L1 tiver quantidade ı́mpar). Por
%exemplo, dividir([1,2,3,4,5],L2,L3) deve retornar L2 = [1, 3, 5], L3 = [2, 4]. Não utilize nenhuma função
%pronta to Prolog para esta tarefa.

dividir(L1,L2,L3) :-
	(tamanhoPar(L1),
	comprimento(L1,Tamanho),
	dividir2(L1,L2,L3,div(Tamanho,2)));
	(tamanhoImpar(L1),
	comprimento(L1,Tamanho),
	dividir2(L1,L2,L3,div(Tamanho,2)+1)).

tamanhoPar([]) :- !.
tamanhoPar([_|T]) :- tamanhoImpar(T).

tamanhoImpar([_]) :- !.
tamanhoImpar([_|T]) :- tamanhoPar(T).

comprimento([],0).

comprimento([H|T],X) :- 
	comprimento(T,NX),
	X is NX + 1.

dividir2(L1,L2,L1,0) :- 
	write('X = '),
	write(L2).

dividir2([H1|T1],L2,L3,Tamanho) :-
	NN is Tamanho-1,
	append(L2,[H1],Resultado),
	dividir2(T1,Resultado,L3,NN).

%?- dividir([1,2,3,4],[],Y).
%X = [1,2]
%Y = [3, 4] .

%?- dividir([1,2,3,4,5],[],Y).
%X = [1,2,3]
%Y = [4, 5] .