%OK

%Crie uma regra inserirElementoPosicao(X,P,L1,L2) que receba um elemento X, uma posição P, e uma
%lista L1 e retorne uma lista L2 onde X é inserido na posição P. Não utilize nenhuma função pronta to
%Prolog para esta tarefa.

inserirElementoPosicao(X,0,L1,L2) :-
	append(L2,[X],Resultado1),
	append(Resultado1,L1,Resultado2),
	resultado(Resultado2).

inserirElementoPosicao(X,P,[H|T],L2) :- 
	CP is P-1,
	append(L2,[H],Resultado),
	inserirElementoPosicao(X,CP,T,Resultado).

resultado(X) :-
	write(X).
	
%?- inserirElementoPosicao(5,1,[1,2,3],[]).
%[1,5,2,3]
%true .