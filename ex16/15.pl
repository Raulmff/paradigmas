%NAO FEITA

%Crie uma regra diferenca(S1,S2,S3), a qual recebe dois conjuntos S1 e S2 e retorna em S3 a diferença
%de S1 e S2. Por exemplo, diferenca([1,2,3,4],[1,2],S3) deve retornar S3 = [3, 4] e diferenca([1,2],[1,2,3,4],S3)
%deve retornar S3 = []. Não utilize nenhuma função pronta to Prolog para esta tarefa.