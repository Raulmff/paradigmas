%OK

%Crie uma regra inverte(L1,L2), a qual recebe uma lista L1 como parâmetro e deve retornar a mesma
%invertida L2. Não utilize nenhuma função pronta do Prolog para realizar esta tarefa.

inverte([],Resp,Resp) :- !.

inverte([H|T],L2,Resp) :- 
	inverte(T,[H|L2],Resp).

%?- inverte([1,2,3],[],X).
%X = [3, 2, 1].