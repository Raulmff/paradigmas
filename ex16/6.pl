%OK

%Crie uma regra menor(L,X), a qual recebe uma lista L de inteiros e retorna o menor elemento da lista.
%Note que X é o menor elemento de L. Retorne 0 caso a lista for vazia. Não utilize nenhuma função
%pronta to Prolog para esta tarefa.

menor([X], X) :- !.
menor([X,Y|T],N) :-
    (X > Y ->
        menor([Y|T],N);
        menor([X|T],N)
    ).

%?- menor([1,2,3],X).
%X = 1.

%?- menor([6,2,3],X).
%X = 2.

%?- menor([6,8,3],X).
%X = 3.