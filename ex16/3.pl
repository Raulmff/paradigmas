%OK

%Crie uma regra numerosParaPalavras(L1,L2) que receba uma lista L1 contendo os números de 0 até 9
%e retorne uma lista L2 que contenha a mesma lista de números de 0 até 9, mas escritos como palavras, ou
%seja, zero, um, dois, tres (sem acento) e assim por diante. Não utilize nenhuma função pronta to Prolog
%para esta tarefa.

nome(0,zero).
nome(1,um).
nome(2,dois).
nome(3,tres).
nome(4,quatro).
nome(5,cinco).
nome(6,seis).
nome(7,sete).
nome(8,oito).
nome(9,nove).

numerosParaPalavras([],L2) :-
	write(L2).

numerosParaPalavras([H|T],L2) :-
	nome(H,Nome),
	append(L2,[Nome],NL2),
	numerosParaPalavras(T,NL2).

%?- numerosParaPalavras([1,2,3,4],[]).
%[um,dois,tres,quatro]
%true.