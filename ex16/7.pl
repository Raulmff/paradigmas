%OK

%Crie uma regra palindrome(L), a qual recebe uma lista L e retorna se ela é uma palı́ndrome. Uma lista é
%uma palı́ndrome se os itens da esquerda para a direita estão na mesma ordem da direita para a esquerda.
%Ex: [1,2,3,4,3,2,1] é uma palı́ndrome. Não utilize nenhuma função pronta to Prolog para esta tarefa.

inverte([],Resp,Resp) :- !.

inverte([H|T],L2,Resp) :- 
	inverte(T,[H|L2],Resp). 

palindrome(L) :-
	inverte(L,X,Resp),
	igual(L,Resp).

igual(L,L).

%?- palindrome([1,2,3]).
%false.

%?- palindrome([1,2,3,2,1]).
%true.