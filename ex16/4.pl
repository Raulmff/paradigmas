%OK

%Crie uma regra soma(L,X), a qual recebe uma lista L de inteiros e retorna a soma de todos os elementos
%da lista. Note que X é o resultado da soma de todos os elementos de L. Retorne 0 caso a lista for vazia.
%Não utilize nenhuma função pronta to Prolog para esta tarefa.

soma([],0).

soma([H|T],X) :- 
	soma(T,NX),
	X is NX + H.

%?- soma([1,2,3],X).
%X = 6.

%?- soma([5,2,3],X).
%X = 10.

%?- soma([5,2,7],X).
%X = 14.