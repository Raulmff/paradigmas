%OK

%Crie uma regra ocorrencias(L,X,N), a qual recebe uma lista L, um elemento X e retorna o número de
%vezes N em que o elemento está presente na lista. Não utilize nenhuma função pronta to Prolog para
%esta tarefa.

ocorrencias([],_,0) :- !.

ocorrencias([H|T],X,N) :-
	H \= X,
    ocorrencias(T,X,N).

ocorrencias([X|T],X,N) :-
	ocorrencias(T,X,NN),
    N is NN + 1.
	
%?- ocorrencias([1,2,3,4,1],1,X).
%X = 2 .

%?- ocorrencias([1,2,3,4,1],3,X).
%X = 1 .

%?- ocorrencias([3,2,3,4,3],3,X).
%X = 3 .