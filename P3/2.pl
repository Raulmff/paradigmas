
ocorrencias([],_,0) :- !.

ocorrencias([H|T],X,N) :-
	H \= X,
    ocorrencias(T,X,N).

ocorrencias([X|T],X,N) :-
	ocorrencias(T,X,NN),
    N is NN + 1.

repetidos(L1,N,L2) :-
	repetidos2(L1,L1,N,L2Novo), !.

repetidos2([],L1,N,L2) :- 
	write(L2).

repetidos2([H|T],L1,N,L2) :-
	ocorrencias(L1,H,NumOcorr),
	(N =< NumOcorr ->
        (append(L2,[H],L2Novo),
        repetidos2(T,L1,N,L2Novo))
        ;
        (repetidos2(T,L1,N,L2))
    ).


%%NAO ESTA 100%, NAO CONSEGUI TIRAR AS REPETICOES, POREM OS NUMEROS ESTAO CERTOS. ELE DA SEMPRE TRUE, NAO SEI POR QUE...

%?- repetidos([1,2,2,3,4,5],1,L).
%[1,2,2,3,4,5]
%true.

%?- repetidos([1,2,2,3,4,5],2,L).
%[2,2]
%true.

%?- repetidos([1,2,2,3,4,5],3,L).
%_L169
%true.

%?- repetidos([1,2,3,2,1,1,2,2,4,5,3],3,L).
%[1,2,2,1,1,2,2]
%true.

%?- repetidos([1,2,3,2,1,1,2,2,4,5,3],2,L).
%[1,2,3,2,1,1,2,2,3]
%true.