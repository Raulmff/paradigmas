
comprimento([],0).

comprimento([H|T],X) :- 
	comprimento(T,NX),
	X is NX + 1.

rotacionarEsquerda(L1,N,L2) :-
	rotacionarEsquerda2(L1,L1,N,L2), !.

rotacionarEsquerda2([H|T],L1,N,L2) :-
	comprimento(L1,Tamanho),
	(N < Tamanho ->
        (nth0(N, L1, Elem),
		append(L2,[Elem],L2Novo),
		NN is N+1,
		rotacionarEsquerda2(T,L1,NN,L2Novo))
        ;
        (Tamanho2 is mod(N,Tamanho),
        nth0(Tamanho2, L1, Elem),
		append(L2,[Elem],L2Novo),
		NN is N+1,
		rotacionarEsquerda2(T,L1,NN,L2Novo))
    ).

rotacionarEsquerda2(_,L1,N,L2) :- 
	comprimento(L2,TamanhoL2),
	comprimento(L1,TamanhoL1),
	TamanhoL1 == TamanhoL2,
	!,
	write(L2).


%% NAO CONSEGUI ATRIBUIR O VALOR PRO L, MAS SE PERCEBER O WRITE FUNCIONA

%?- rotacionarEsquerda([1,2,3,4,5,6,7],2,L).
%[3,4,5,6,7,1,2]
%L = [].

%?- rotacionarEsquerda([1,2,3,4,5,6,7],1,L).
%[2,3,4,5,6,7,1]
%L = [].