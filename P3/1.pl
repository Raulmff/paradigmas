genitor(fred, maria).
genitor(pam, bob).
genitor(tom, bob).
genitor(pam, liz).
genitor(tom, liz).

genitor(bob, ana).
genitor(bob, pat).
genitor(bia, ana).
genitor(bia, pat).

genitor(liz,bill).
genitor(trump,bill).

genitor(mary,tim).
genitor(trump,tim).

genitor(pat, jim).
genitor(kim, jim).

mulher(maria).
mulher(pam).
mulher(bia).
mulher(liz).
mulher(mary).
mulher(ana).
mulher(pat).

homem(fred).
homem(tom).
homem(bob).
homem(trump).
homem(kim).
homem(bill).
homem(tim).
homem(jim).

idade(fred, 56).
idade(maria, 30).
idade(pam, 98).
idade(tom, 70).
idade(bob, 45).
idade(bia, 43).
idade(liz, 47).
idade(trump, 54).
idade(mary, 51).
idade(ana, 12).
idade(pat, 25).
idade(kim, 27).
idade(bill, 15).
idade(tim, 17).
idade(jim, 1).

irmao(X,Y) :- genitor(PaiAmbos, X), genitor(PaiAmbos, Y), X \== Y, homem(X).
irma(X,Y) :- genitor(PaiAmbos, X), genitor(PaiAmbos, Y), X \== Y, mulher(X).
irmaos(X,Y) :- (irmao(X,Y); irma(X,Y)), X \== Y.

%A: casados(X,Y) onde X eh casado com Y se possuirem filhos em comum
casados(X,Y) :-
	genitor(X,FilhoIgual),
	genitor(Y,FilhoIgual),
	X \= Y.

%?- casados(pam,tom).
%true .

%?- casados(bia,bob).
%true .

%?- casados(bob,liz).
%false.

%B: cunhados(X,Y) onde X eh cunhado ou cunhada de Y
cunhados(X,Y) :-
	casados(X,Z),
	irmaos(Y,Z),
	X \= Y.

%?- cunhados(bia,liz).
%true .

%?- cunhados(trump,bob).
%true .

%?- cunhados(liz,bill).
%false.

%C: sobrinho(X,Y) onde X eh sobrinho de Y e sobrinha
sobrinho(X,Y) :-
	genitor(PaiDeX,X),
	irmaos(PaiDeX,Y),
	X \= Y,
	homem(X).

%?- sobrinho(bill,bob).
%true .

%?- sobrinho(jim,ana).
%true .

%?- sobrinho(pat,liz).
%false.

sobrinha(X,Y) :-
	genitor(PaiDeX,X),
	irmaos(PaiDeX,Y),
	X \= Y,
	mulher(X).

%?- sobrinha(ana,liz).
%true .

%?- sobrinha(pat,liz).
%true .

%?- sobrinha(bob,tom).
%false.

%D: casadoMaisVezes(X) se X tem mais de um filho com duas pessoas
casadoMaisVezes(X) :-
	casados(X,Conjuge1),
	casados(X,Conjuge2),
	Conjuge1 \= Conjuge2.

%?- casadoMaisVezes(trump).
%true .

%?- casadoMaisVezes(ana).
%false.

avo(AvoX, X) :- genitor(GenitorX, X), genitor(AvoX, GenitorX), homem(AvoX).
avoh(AvohX, X) :- genitor(GenitorX, X), genitor(AvohX, GenitorX), mulher(AvohX).
avos(Avo,X) :- avo(Avo,X) ; avoh(Avo,X).

%E: consulta para lista de (sem rep) avo ou avoh com mais de 50
%?- findall(Avo,((avo(Avo,X);avoh(Avo,X)),idade(Avo,Idade),Idade>50),Avos),sort(Avos,AvosOrdenados).
%Avos = [tom, tom, tom, pam, pam, pam],
%AvosOrdenados = [pam, tom].

%F: consulta para lista ordenada sem rep de nao avos ou avohs com filhos entre 30 e 90 inclusive
%?- findall(Pessoa,(genitor(Pessoa,Filho),idade(Filho,Idade),(Idade>=30,Idade=<90),not(avo(Pessoa,X)),not(avoh(Pessoa,X))),Pessoas),sort(Pessoas,PessoasOrd).
%Pessoas = PessoasOrd, PessoasOrd = [fred].

%G: consulta para retorne netos agrupados por avo
%?- setof(Neto,(avo(Avo,Neto)),Netos).
%Avo = bob,
%Netos = [jim] ;
%Avo = tom,
%Netos = [ana, bill, pat].