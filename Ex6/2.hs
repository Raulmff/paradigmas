-- Altere nosso exemplo da forma e inclua uma nova forma (Triangulo) no construtor do tipo Forma e também
-- calcule sua área.

--OK

data Forma = Circulo Float | Retangulo Float Float | Triangulo Float Float

area :: Forma -> Float
area (Triangulo a b) = (a*b) / 2.0

main = do
	print (area (Triangulo 5 2))
