-- Crie um tipo de dados Aluno, usando type, assim como criamos um tipo de dados Pessoa. O tipo Aluno
-- deve possuir um campo para o nome, outro para a disciplina e outros três campos para notas. Agora,
-- execute os passos abaixo:

--FINALIZAR

type Nome = String
type Disciplina = String
type Nota = Float
type Aluno = (Nome, Disciplina, Nota, Nota)

-- A: Crie uma função no mesmo estilo que a função pessoa, vista em sala e disponı́vel nos slides no Moodle,
-- ou seja, que receba um inteiro e retorne um Aluno correspondente ao valor inteiro.
-- B: Crie alguns alunos de exemplo, assim como também feito no exemplo da pessoa.
retornaAluno :: Int -> Aluno
retornaAluno 1 = ("Amanda", "Paradigmas de Programacao", 8.0, 9.0)
retornaAluno 2 = ("Bruno", "Linguagens Formais", 2.0, 4.0)
retornaAluno 3 = ("Carlos", "Linguagens Formais", 5.5, 8.0)

-- C: No main, imprima o primeiro nome de um aluno, portanto crie uma função para obter o primeiro
-- nome.
getNome :: Aluno -> Nome
getNome (n,_,_,_) = n

-- D: Crie uma função que receba um Int e retorne a média do aluno correspondente.
getMedia :: Aluno -> Float
getMedia (_,_,n1,n2) = (n1 + n2) / 2.0


--FALTA FINALIZAR
-- E: Crie uma função que calcule a média da turma, ou seja, considerando todos os alunos. DICA: crie
-- uma função recursiva que receba o primeiro identificador de aluno e incremente o identificador a cada
-- chamada recursiva, até chegar no último aluno. Não use listas!
mediaTurma :: Int -> Float -> Float
mediaTurma 3 soma = ( soma + (getMedia (retornaAluno 3)) :: Float ) / 3.0
mediaTurma i soma = ( mediaTurma (i+1) (soma + (getMedia (retornaAluno i))) )

main = do
	-- print "Questao A e B:"
	-- print (retornaAluno 1)
	-- print (retornaAluno 2)
	-- print (retornaAluno 3)

	-- print "Questao C:"
	-- print (getNome (retornaAluno 1))
	-- print (getNome (retornaAluno 2))

	-- print "Questao D:"
	-- print (getMedia (retornaAluno 1))
	-- print (getMedia (retornaAluno 2))

	print "Questao E:"
	print (mediaTurma 1 0)