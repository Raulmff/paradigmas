-- Motifique o arquivo arvore.hs (disponı́vel no Moodle) de forma a adicionar novas operações a nossa
-- árvore:

--OK

data Arvore = Null | No Int Arvore Arvore

minhaArvore :: Arvore
minhaArvore = No 52 (No 32 (No 12 Null Null) (No 35 Null Null)) (No 56 (No 55 Null Null) (No 64 Null Null))

somaElementos :: Arvore -> Int
somaElementos Null = 0
somaElementos (No n esq dir) = n + (somaElementos esq) + (somaElementos dir)

buscaElemento :: Arvore -> Int -> Bool
buscaElemento Null _ = False
buscaElemento (No n esq dir) x 
    | (n == x) = True                           
    | otherwise = (buscaElemento esq x) || (buscaElemento dir x)

limiteSup :: Int
limiteSup = 1000 --Define um limite superior para o maior número

minimo :: Int -> Int -> Int
minimo x y | (x < y) = x
           | otherwise = y

minimoElemento :: Arvore -> Int
minimoElemento Null = limiteSup 
minimoElemento (No n esq dir) = 
    minimo n (minimo (minimoElemento esq) (minimoElemento dir))

-- A: Crie uma função com a seguinte assinatura: ocorrenciasElemento :: Arvore -> Int -> Int, a
-- qual recebe um número e deve retornar a quantidade de ocorrências dele na árvore.                           
ocorrenciasElemento :: Arvore -> Int -> Int
ocorrenciasElemento Null _ = 0
ocorrenciasElemento (No n esq dir) x = 
	if (n==x) 
		then
			1 + ocorrenciasElemento esq x + ocorrenciasElemento dir x
		else 
			ocorrenciasElemento esq x + ocorrenciasElemento dir x

-- B: Crie uma função com a seguinte assinatura: maioresQueElemento :: Arvore -> Int -> Int, a
-- qual recebe um número e deve retornar a quantidade de números maiores que ele na árvore.
maioresQueElemento :: Arvore -> Int -> Int
maioresQueElemento Null _ = 0
maioresQueElemento (No n esq dir) x = 
	if (n>x) 
		then
			1 + maioresQueElemento esq x + maioresQueElemento dir x
		else 
			maioresQueElemento esq x + maioresQueElemento dir x

-- C: Crie uma função com a seguinte assinatura: mediaElementos :: Arvore -> Float, a qual deve
-- retornar a média dos números na árvore. DICA: utilize a função fromIntegral para converter um
-- tipo inteiro para um tipo compatı́vel com o operador de divisão /
-- D: Crie uma função com a seguinte assinatura: quantidade ::
-- a quantidade de elementos na árvore.
quantidade :: Arvore -> Int
quantidade Null = 0
quantidade (No n esq dir) = 1 + quantidade esq + quantidade dir

mediaElementos :: Arvore -> Float
mediaElementos Null = 0
mediaElementos (No n esq dir) = (fromIntegral (somaElementos (No n esq dir)) :: Float) / (fromIntegral (quantidade (No n esq dir)) :: Float)

-- E: Crie uma função com a seguinte assinatura: elementos ::
-- uma lista com todos os elementos na árvore.
elementos :: Arvore -> [Int]
elementos Null = []
elementos (No n esq dir) = [n] ++ elementos esq ++ elementos dir

main = do 
	-- putStrLn (show (somaElementos minhaArvore))
    -- putStrLn (show (buscaElemento minhaArvore 30))
    -- putStrLn (show (buscaElemento minhaArvore 55))
    -- putStrLn (show (minimoElemento minhaArvore))
    --print (ocorrenciasElemento minhaArvore 52)
    --print (maioresQueElemento minhaArvore 52)
    --print (quantidade minhaArvore)
    --print (mediaElementos minhaArvore)
    print (elementos minhaArvore)