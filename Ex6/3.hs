-- Crie um novo tipo Ponto, usando data, que pode ser um ponto 2D ou um ponto 3D. Depois, crie uma
-- função que receba dois pontos (necessariamente ambos sendo 2D ou ambos sendo 3D), e retorne a distância
-- entre eles.

--OK

data Forma = Ponto2 Float Float | Ponto3 Float Float Float

distancia :: Forma -> Forma -> Float
--
distancia (Ponto2 a b) (Ponto2 c d) = ( sqrt ((c-a)**2 + (d-b)**2) 	)
distancia (Ponto3 a b c) (Ponto3 d e f) = (sqrt ((d-a)**2 + (e-b)**2 + (f-c)**2))

main = do
	print (distancia (Ponto2 (-2) 4) (Ponto2 2 2))
	print (distancia (Ponto3 4 (-8) (-9)) (Ponto3 2 (-3) (-5)))